#ifndef TimeCounter_h
#define TimeCounter_h

#if ENABLE(AOT_PROFILE)

#include <wtf/Vector.h>

namespace JSC {

class TimeCounter {
public:
    enum Type {
        TYPEwebload,
        TYPEjsexec,
        TYPEjscall,
        TYPEdfgjit,
        TYPEaotload,
        TYPEaotstore,
        TYPEsize
    };

    static void start(Type type);
    static void stop(Type type);

    static void start_load();
    static void stop_load();

    static void countByte();
    static void countBase();
    static void countDFG();

    static void reset();
    static void report();

private:
    static double accumTime[TYPEsize];
    static double startTime[TYPEsize];
    static unsigned byteCount;
    static unsigned baseCount;
    static unsigned dfgCount;
    static int level[TYPEsize];
};

}

#endif // AOT_PROFILE
#endif // TimeCounter_h
