#include "config.h"
#include "TimeCounter.h"

#if ENABLE(AOT_PROFILE)

#include "wtf/CurrentTime.h"

namespace JSC {

double TimeCounter::accumTime[TimeCounter::TYPEsize];
double TimeCounter::startTime[TimeCounter::TYPEsize];
unsigned TimeCounter::byteCount;
unsigned TimeCounter::baseCount;
unsigned TimeCounter::dfgCount;
int TimeCounter::level[TimeCounter::TYPEsize];

void TimeCounter::start(Type type)
{
    ASSERT(!startTime[type]);
    
    if (LIKELY(!level[type]))
        startTime[type] = currentTimeMS();

    level[type]++;
}

void TimeCounter::stop(Type type)
{
    ASSERT(startTime[type]);

    if (LIKELY(!--level[type])) {
        double elapsed = currentTimeMS() - startTime[type];

        accumTime[type] += elapsed;
        startTime[type] = 0;
    }
}

void TimeCounter::start_load()
{
    printf("===========================================\n");
    startTime[TYPEwebload] = currentTimeMS();
}

void TimeCounter::stop_load()
{
    double elapsed = currentTimeMS() - startTime[TYPEwebload];
    accumTime[TYPEwebload] = elapsed;
}

void TimeCounter::countByte()
{
    byteCount++;
}

void TimeCounter::countBase()
{
    baseCount++;
}

void TimeCounter::countDFG()
{
    dfgCount++;
}

void TimeCounter::reset()
{
    for (int i = 0; i < TYPEsize; i++) {
        startTime[i] = 0;
        accumTime[i] = 0;
        level[i] = 0;
    }

    byteCount = 0;
    baseCount = 0;
    dfgCount = 0;
}

void TimeCounter::report()
{
    printf("---------- REPORT TIME ----------\n");
    printf("WebLoadingTime: %f \n", accumTime[TYPEwebload]);
    printf("JSExecutionTime: %f \n", accumTime[TYPEjsexec]);
    printf("JSFunctionTime: %f \n", accumTime[TYPEjscall]);
    printf("DFGJITTime: %f \n", accumTime[TYPEdfgjit]);
    printf("AOTLoadTime: %f \n", accumTime[TYPEaotload]);
    printf("AOTStoreTime: %f \n", accumTime[TYPEaotstore]);
    printf("BytecodeCount: %d\n", byteCount);
    printf("BaselineJITCount: %d\n", baseCount);
    printf("DFGJITCount: %d\n", dfgCount);
}

}
#endif // AOT_PROFILE
