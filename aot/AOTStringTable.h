/*
 * Copyright (C) 2011 Apple Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AOTStringTable_h
#define AOTStringTable_h

#if ENABLE(DFG_AOT)

#include <wtf/Vector.h>
#include <wtf/RefPtr.h>
#include <wtf/RefCounted.h>
#include <wtf/text/StringImpl.h>

namespace JSC {

class VM;

class AOTStringTable : public RefCounted<AOTStringTable> {
    WTF_MAKE_FAST_ALLOCATED;
public:
    AOTStringTable(StringImpl* empty) 
        : m_emptyString(empty)
        , m_maxLength(0)
    { 
    }

    ~AOTStringTable() { clear(); }

    void clear() { m_table.clear(); }

    bool validate(StringImpl* impl);
    void initializeAdd(StringImpl* impl);
    unsigned add(StringImpl* impl);
    StringImpl* get(unsigned index); 

    void writeToFile(FILE* file);
    void loadFromFile(VM* vm, FILE* file);

private:
    Vector<RefPtr<StringImpl>> m_table;

    StringImpl* m_emptyString;
    unsigned m_maxLength;
};

}

#endif

#endif // AOTStringTable_h
