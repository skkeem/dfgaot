#include "config.h"

#if ENABLE(DFG_AOT) 

#include "AOT.h"
#include "VM.h"
#include "CommonIdentifiers.h"
#include "AOTLoaderWriter.h"
#include "AOTStringTable.h"
#include "SourceProvider.h"
#include "LinkBuffer.h"
#include "TimeCounter.h"
#include "JITOperations.h"
#include "DFGOperations.h"
#include <wtf/RefCounted.h>

#define URLSIZE 500

namespace JSC {

const char* AOT::filePathPrefix = "AOTDir/";
const char* AOT::byteMetaFileSuffix = "_byte_meta";
const char* AOT::dfgMetaFileSuffix = "_dfg_meta";
const char* AOT::byteDataFileSuffix = "_byte_data";
const char* AOT::dfgDataFileSuffix = "_dfg_data";
const char* AOT::stringFileSuffix = "_string";

AOT::AOT(VM* vm)
    : m_vm(vm)
    , m_storedOnce(false)
    , m_storeByteAOTCount(0)
    , m_storeDFGAOTCount(0)
    , m_failedByteAOTCount(0)
    , m_failedDFGAOTCount(0)
{
    m_byteWriter = new AOTWriter(vm, this);
    m_dfgWriter = new AOTWriter(vm, this);
    m_byteLoader = new AOTLoader(vm, this);
    m_dfgLoader = new AOTLoader(vm, this);

#define FUNCTIONDEF(func) \
    m_functionList.append((void*) (func));
#include "JSCFunction.list"
#include "DFGFunction.list"
#undef FUNCTIONDEF

}

AOT::~AOT()
{
    delete m_byteWriter;
    delete m_dfgWriter;
    delete m_byteLoader;
    delete m_dfgLoader;

    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.begin();
    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator end = m_fileNames.end();
    for(; iter != end; ++iter)
        delete iter->value;
}

Ref<AOT> AOT::create(VM* vm)
{
    Ref<AOT> result = adoptRef(*new AOT(vm));
    return result;
}

void AOT::storeFunctionByte(AOTKey key, UnlinkedFunctionExecutable* executable, UnlinkedFunctionCodeBlock* codeBlock)
{
    TimeCounter::start(TimeCounter::TYPEaotstore);

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length());
    ASSERT(provider->url().length() < URLSIZE);

    registerFileNames(provider);

    ASSERT(m_cachedByteMetaInfos.find(key) == m_cachedByteMetaInfos.end()); 

    m_byteWriter->setAOTStringTable(addStringTable(provider));
    if (!m_byteWriter->writeFunctionCodeBlock(executable, codeBlock)) {
        m_failedByteAOTCount++;
        m_byteWriter->clear();
        TimeCounter::stop(TimeCounter::TYPEaotstore);
        return;
    }

    auto addResult = m_offsetInByteDataFiles.add(provider, 0);
    if (addResult.isNewEntry && !m_storedOnce) {
        m_storedOnce = true;
        removeFiles(provider);
    }

    ASSERT(m_offsetInByteDataFiles.find(provider) != m_offsetInByteDataFiles.end());

    AOTMetaInfo meta(addResult.iterator->value, m_byteWriter->bufferSize(), AOT_BYTE);
    m_cachedByteMetaInfos.add(key, meta);
    addResult.iterator->value += m_byteWriter->bufferSize();

    writeToFile(key, meta, false);

    m_byteWriter->clear();
    m_storeByteAOTCount++;

    TimeCounter::stop(TimeCounter::TYPEaotstore);
}

void AOT::storeProgramByte(AOTKey key, ProgramExecutable* executable, UnlinkedProgramCodeBlock* codeBlock)
{
    TimeCounter::start(TimeCounter::TYPEaotstore);

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length());
    ASSERT(provider->url().length() < URLSIZE);

    registerFileNames(provider);

    ASSERT(m_cachedByteMetaInfos.find(key) == m_cachedByteMetaInfos.end()); 

    m_byteWriter->setAOTStringTable(addStringTable(provider));
    if (!m_byteWriter->writeProgramCodeBlock(executable, codeBlock)) {
        m_failedByteAOTCount++;
        m_byteWriter->clear();
        TimeCounter::stop(TimeCounter::TYPEaotstore);
        return;
    }

    auto addResult = m_offsetInByteDataFiles.add(provider, 0);
    if (addResult.isNewEntry && !m_storedOnce) {
        m_storedOnce = true;
        removeFiles(provider);
    }

    ASSERT(m_offsetInByteDataFiles.find(provider) != m_offsetInByteDataFiles.end());

    AOTMetaInfo meta(addResult.iterator->value, m_byteWriter->bufferSize(), AOT_BYTE);
    m_cachedByteMetaInfos.add(key, meta);
    addResult.iterator->value += m_byteWriter->bufferSize();

    writeToFile(key, meta, false);

    m_byteWriter->clear();
    m_storeByteAOTCount++;

    TimeCounter::stop(TimeCounter::TYPEaotstore);
}

void AOT::storeDFG(AOTKey key, DFG::JITCompiler& jit, bool isGlobalCode)
{
    TimeCounter::start(TimeCounter::TYPEaotstore);

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length());
    ASSERT(provider->url().length() < URLSIZE);

    registerFileNames(provider);

    if (m_cachedDFGMetaInfos.find(key) != m_cachedDFGMetaInfos.end()) {
        puts("already DFG AOTCed");

        //bool removed = m_cachedDFGMetaInfos.remove(key);
        //ASSERT(removed);
        TimeCounter::stop(TimeCounter::TYPEaotstore);
        return;
    }

    if (isGlobalCode)
        printf("GlobalCode store\n");

    m_dfgWriter->setAOTStringTable(addStringTable(provider));
    m_dfgWriter->writeDFGJITCode(jit);
    if (!m_dfgWriter->writeDFGJITInfo(jit, isGlobalCode)) {
        m_failedDFGAOTCount++;
        m_dfgWriter->clear();
        TimeCounter::stop(TimeCounter::TYPEaotstore);
        return;
    }

    auto addResult = m_offsetInDFGDataFiles.add(provider, 0);
    if (addResult.isNewEntry && !m_storedOnce) {
        m_storedOnce = true;
        removeFiles(provider);
    }

    ASSERT(m_offsetInDFGDataFiles.find(provider) != m_offsetInDFGDataFiles.end());

    AOTMetaInfo meta(addResult.iterator->value, m_dfgWriter->bufferSize(), AOT_DFG);
    m_cachedDFGMetaInfos.add(key, meta);
    addResult.iterator->value += m_dfgWriter->bufferSize();

    writeToFile(key, meta, true);

    m_dfgWriter->clear();
    m_storeDFGAOTCount++;

    TimeCounter::stop(TimeCounter::TYPEaotstore);
}

void AOT::writeToFile(AOTKey& key, AOTMetaInfo& meta, bool isDFG)
{
    SourceProvider* provider = key.provider();

    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.find(provider);
    ASSERT(iter != m_fileNames.end());

    FILE* metaFile = isDFG? fopen(iter->value->dfgMetaFileName, "ab") : fopen(iter->value->byteMetaFileName, "ab");
    ASSERT(!!metaFile);
    int startOffset = key.startOffset();
    bool isCodeForConstruct = key.isCodeForConstruct();
    fwrite(&startOffset, sizeof(int), 1, metaFile);
    fwrite(&isCodeForConstruct, sizeof(bool), 1, metaFile);
    fwrite(&meta, sizeof(AOTMetaInfo), 1, metaFile);
    fclose(metaFile);

    FILE* dataFile = isDFG? fopen(iter->value->dfgDataFileName, "ab") : fopen(iter->value->byteDataFileName, "ab");
    ASSERT(!!dataFile);
    AOTWriter* writer = isDFG? m_dfgWriter : m_byteWriter;
    fwrite(writer->bufferData(), sizeof(char), writer->bufferSize(), dataFile);
    fclose(dataFile);
}

void AOT::loadMetaInfos(SourceProvider* provider)
{
    TimeCounter::start(TimeCounter::TYPEaotload);
   
    ASSERT(provider->url().length() < URLSIZE);

    registerFileNames(provider);

    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.find(provider);
    ASSERT(iter != m_fileNames.end());

    int startOffset;
    bool isCodeForConstruct;
    AOTMetaInfo meta;

    // Byte MetaFile
    if (Options::loadByteAOT()) {
        FILE* byteMetaFile = fopen(iter->value->byteMetaFileName, "rb");
        /*
        if (!byteMetaFile) {
            puts("Byte MetaFile does not exist");
            TimeCounter::stop(TimeCounter::TYPEaotload);
            return;
        }
        */
        if (byteMetaFile) {
            while (!feof(byteMetaFile)) {
                fread(&startOffset, sizeof(int), 1, byteMetaFile);
                fread(&isCodeForConstruct, sizeof(bool), 1, byteMetaFile);
                int check = fread(&meta, sizeof(AOTMetaInfo), 1, byteMetaFile);
    
                if (!check)
                    break;

                ASSERT(m_byteMetaInfos.find(AOTKey(provider, startOffset, isCodeForConstruct)) == m_byteMetaInfos.end());
                m_byteMetaInfos.add(AOTKey(provider, startOffset, isCodeForConstruct), meta);
            }

            fclose(byteMetaFile);
        }
    }

    // DFG Meta File
    if (Options::loadAOT()) {
        FILE* dfgMetaFile = fopen(iter->value->dfgMetaFileName, "rb");
        /*
        if (!dfgMetaFile) {
            puts("DFG MetaFile does not exist");
            TimeCounter::stop(TimeCounter::TYPEaotload);
            return;
        }
        */

        if (dfgMetaFile) {

            while (!feof(dfgMetaFile)) {
                fread(&startOffset, sizeof(int), 1, dfgMetaFile);
                fread(&isCodeForConstruct, sizeof(bool), 1, dfgMetaFile);
                int check = fread(&meta, sizeof(AOTMetaInfo), 1, dfgMetaFile);

                if (!check)
                    break;

                ASSERT(m_dfgMetaInfos.find(AOTKey(provider, startOffset, isCodeForConstruct)) == m_dfgMetaInfos.end());
                m_dfgMetaInfos.add(AOTKey(provider, startOffset, isCodeForConstruct), meta);
            }

            fclose(dfgMetaFile);
        }
    }

    loadStringTable(provider);

    TimeCounter::stop(TimeCounter::TYPEaotload);
}

//TODO
UnlinkedFunctionCodeBlock* AOT::loadFunctionByte(AOTKey key, UnlinkedFunctionExecutable* executable)
{
    TimeCounter::start(TimeCounter::TYPEaotload);

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length() && provider->url().length() < URLSIZE);
    ASSERT(m_byteMetaInfos.find(key) != m_byteMetaInfos.end());

    AOTMetaInfo meta = m_byteMetaInfos.find(key)->value;

    loadFromFile(key, meta, false); 

    m_byteLoader->setAOTStringTable(addStringTable(provider));
    UnlinkedFunctionCodeBlock* result = m_byteLoader->loadFunctionCodeBlock(executable);

    ASSERT(m_byteLoader->bufferIndex() == meta.dataSize);

    TimeCounter::stop(TimeCounter::TYPEaotload);

    return result;
}

UnlinkedProgramCodeBlock* AOT::loadProgramByte(AOTKey key, ProgramExecutable* executable)
{
    TimeCounter::start(TimeCounter::TYPEaotload);

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length() && provider->url().length() < URLSIZE);
    ASSERT(m_byteMetaInfos.find(key) != m_byteMetaInfos.end());

    AOTMetaInfo meta = m_byteMetaInfos.find(key)->value;

    loadFromFile(key, meta, false); 

    m_byteLoader->setAOTStringTable(addStringTable(provider));
    UnlinkedProgramCodeBlock* result = m_byteLoader->loadProgramCodeBlock(executable);

    ASSERT(m_byteLoader->bufferIndex() == meta.dataSize);

    TimeCounter::stop(TimeCounter::TYPEaotload);

    return result;
}

void AOT::loadDFG(AOTKey key, CodeBlock* codeBlock, bool isGlobalCode)
{
    TimeCounter::start(TimeCounter::TYPEaotload);

    if (isGlobalCode)
        printf("GlobalCode DFG load\n");

    SourceProvider* provider = key.provider();

    ASSERT(provider);
    ASSERT(provider->url().length() && provider->url().length() < URLSIZE);
    ASSERT(m_dfgMetaInfos.find(key) != m_dfgMetaInfos.end());

    AOTMetaInfo meta = m_dfgMetaInfos.find(key)->value;

    loadFromFile(key, meta, true); 

    auto linkBuffer = std::make_unique<LinkBuffer>(*m_vm, codeBlock);

    m_dfgLoader->setAOTStringTable(addStringTable(provider));
    m_dfgLoader->loadDFGJITCode(*linkBuffer, codeBlock);
    m_dfgLoader->loadDFGJITInfo(*linkBuffer, codeBlock, isGlobalCode);

    if (isGlobalCode)
        m_dfgLoader->finalize(*linkBuffer, codeBlock);
    else
        m_dfgLoader->finalizeFunction(*linkBuffer, codeBlock);

    ASSERT(m_dfgLoader->bufferIndex() == meta.dataSize);

    TimeCounter::stop(TimeCounter::TYPEaotload);
}

void AOT::loadFromFile(AOTKey& key, AOTMetaInfo& meta, bool isDFG)
{
    SourceProvider* provider = key.provider();

    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.find(provider);
    ASSERT(iter != m_fileNames.end());

    FILE* dataFile = isDFG? fopen(iter->value->dfgDataFileName, "rb") : fopen(iter->value->byteDataFileName, "rb");
    ASSERT(!!dataFile);
    fseek(dataFile, meta.dataOffset, SEEK_SET);

    AOTLoader* loader = isDFG? m_dfgLoader : m_byteLoader;
    loader->loadDataFile(dataFile, meta.dataSize);
    fclose(dataFile);
}

void AOT::removeFiles(SourceProvider* provider)
{
    ASSERT(provider->url().length() && provider->url().length() < URLSIZE);

    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.find(provider);
    ASSERT(iter != m_fileNames.end());

    remove(iter->value->byteMetaFileName);
    remove(iter->value->dfgMetaFileName);
    remove(iter->value->byteDataFileName);
    remove(iter->value->dfgDataFileName);
    remove(iter->value->stringFileName);
}

void AOT::registerFileNames(SourceProvider* provider)
{
    ASSERT(provider->url().length() && provider->url().length() < URLSIZE);

    if (m_fileNames.find(provider) != m_fileNames.end())
        return;

    char sourceURL[512];
    char file[512];

    AOTFileName* fileName = new AOTFileName();
    
    strcpy(file, filePathPrefix);
    strcpy(sourceURL, provider->url().utf8().data());

    char* pch;
    while (pch = strchr(sourceURL, '/'))
        *pch = '_';
    pch = strchr(sourceURL, '_');

    if (pch)
        strcat(file, pch + 1);
    else
        strcat(file, sourceURL);

    strcpy(fileName->byteMetaFileName, file);
    strcpy(fileName->dfgMetaFileName, file);
    strcpy(fileName->byteDataFileName, file);
    strcpy(fileName->dfgDataFileName, file);
    strcpy(fileName->stringFileName, file);

    strcat(fileName->byteMetaFileName, byteMetaFileSuffix);
    strcat(fileName->dfgMetaFileName, dfgMetaFileSuffix);
    strcat(fileName->byteDataFileName, byteDataFileSuffix);
    strcat(fileName->dfgDataFileName, dfgDataFileSuffix);
    strcat(fileName->stringFileName, stringFileSuffix);

    auto fileResult = m_fileNames.add(provider, fileName);
    ASSERT(fileResult.isNewEntry);
}

AOTStringTable* AOT::addStringTable(SourceProvider* provider)
{
    auto addResult = m_stringTableMap.add(provider, nullptr);
    if (addResult.isNewEntry)
        addResult.iterator->value = adoptRef(new AOTStringTable(m_vm->propertyNames->emptyIdentifier.impl()));
    return addResult.iterator->value.get();
}

void AOT::storeStringTableMap()
{
    StringTableMap::iterator iter = m_stringTableMap.begin();
    StringTableMap::iterator end = m_stringTableMap.end();

    for (; iter != end; ++iter) {
        SourceProvider* provider = iter->key.get();
        AOTStringTable* table = iter->value.get(); 

        HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iterFile = m_fileNames.find(provider);
        ASSERT(iterFile != m_fileNames.end());

        FILE* stringFile = fopen(iterFile->value->stringFileName, "w");
        ASSERT(!!stringFile);
        table->writeToFile(stringFile);
        fclose(stringFile);
    }

}

void AOT::loadStringTable(SourceProvider* provider)
{
    HashMap<RefPtr<SourceProvider>, AOTFileName*>::iterator iter = m_fileNames.find(provider);
    ASSERT(iter != m_fileNames.end());

    FILE* stringFile = fopen(iter->value->stringFileName, "r");
    if (!stringFile)
        return;

    ASSERT(m_stringTableMap.find(provider) == m_stringTableMap.end());
    AOTStringTable* table = addStringTable(provider);
    table->loadFromFile(m_vm, stringFile);
    fclose(stringFile);
}

unsigned AOT::getFunctionIndex(void* func)
{
    ASSERT(m_functionList.contains(func));
    return m_functionList.find(func);
}

void* AOT::getFunctionPtr(unsigned index)
{
    ASSERT(index < m_functionList.size());
    return m_functionList[index];
}

void AOT::printAOTCount()
{
    printf("StoreByteAOTCount: %d\n", m_storeByteAOTCount);
    printf("StoreDFGAOTCount: %d\n", m_storeDFGAOTCount);
    printf("FailedByteAOTCount: %d\n", m_failedByteAOTCount);
    printf("FailedDFGAOTCount: %d\n\n", m_failedDFGAOTCount);
}

static AOT* theGlobalAOT;

AOT* ensureGlobalAOT(VM* vm)
{   
    static std::once_flag initializeGlobalAOTOnceFlag;
    std::call_once(initializeGlobalAOTOnceFlag, [] (VM* vm) {
        theGlobalAOT = &AOT::create(vm).leakRef();
    }, vm);
    return theGlobalAOT;
}

AOT* existingGlobalAOT()
{
    return theGlobalAOT;
}

#ifndef NDEBUG
static bool inDFGCompilation;

void setInDFGCompilation(bool set)
{
    inDFGCompilation = set;
}

bool isInDFGCompilation()
{
    return inDFGCompilation;
}
#endif

static bool recycleFlag = false;
bool recycleMode()
{
    return recycleFlag;
}

void recycleModeSet(bool n)
{
    recycleFlag = n;
}
}

#endif
/* END DFG_AOT */
