/*
 * Copyright (C) 2011 Apple Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
 
#include "config.h"

#if ENABLE(DFG_AOT)
#include "AOTStringTable.h"
#include "Identifier.h"

namespace JSC {

bool AOTStringTable::validate(StringImpl* impl)
{
    if (UNLIKELY(!impl->isAtomic() || !impl->length() || !impl->is8Bit())) {
        puts("Invalidate StringImpl -> failed AOTC");
        return false;
    }

    return true;
}

void AOTStringTable::initializeAdd(StringImpl* impl)
{
    ASSERT(!m_table.contains(impl));
    m_table.append(impl);
}

unsigned AOTStringTable::add(StringImpl* impl)
{
    ASSERT(impl->isAtomic());
    ASSERT(impl->length());
    ASSERT(impl->is8Bit());

    if (!m_table.contains(impl)) {
        m_table.append(impl);

        if (m_maxLength < impl->length())
            m_maxLength = impl->length();
    }

    ASSERT(m_table.contains(impl));
    return m_table.find(impl);
}

StringImpl* AOTStringTable::get(unsigned index) 
{ 
    if (UNLIKELY(index == UINT_MAX))
        return m_emptyString;

    ASSERT(index < m_table.size());
    return m_table[index].get(); 
}

void AOTStringTable::writeToFile(FILE* file)
{
    unsigned size = m_table.size();
    unsigned maxLength = m_maxLength;
    unsigned length;

    fwrite(&size, sizeof(unsigned), 1, file);
    fwrite(&maxLength, sizeof(unsigned), 1, file);

    for (unsigned i = 0; i < m_table.size(); i++) {
        ASSERT(m_table[i]->isAtomic());
        ASSERT(m_table[i]->length());
        ASSERT(m_table[i]->is8Bit());
        length = m_table[i]->length();

        fwrite(&length, sizeof(unsigned), 1, file);
        fwrite(m_table[i]->utf8().data(), sizeof(char), length, file);
    }

    /*
    // debug
    FILE* debugFile = fopen("debugFile", "w");
    for (unsigned i = 0; i < m_table.size(); i++) {
        ASSERT(m_table[i]->length());
        ASSERT(m_table[i]->is8Bit());

        fprintf(debugFile, "%s\n", m_table[i]->utf8().data());
    }
    fclose(debugFile);
    */
}

void AOTStringTable::loadFromFile(VM* vm, FILE* file)
{
    unsigned size, maxLength, length;
    char* buffer;

    fread(&size, sizeof(unsigned), 1, file);
    fread(&maxLength, sizeof(unsigned), 1, file);

    buffer = new char[maxLength];
    for (unsigned i = 0; i < size; i++) {
        fread(&length, sizeof(unsigned), 1, file);
        fread(buffer, sizeof(char), length, file);
        //buffer[length] = '\0';
        ASSERT(length);

        Identifier ident(vm, reinterpret_cast<const LChar*>(buffer), length);
        initializeAdd(ident.impl());
    }

    delete[] buffer;
}

}

#endif
