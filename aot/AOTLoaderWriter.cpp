#include "config.h"

#if ENABLE(DFG_AOT) 

#include "AOT.h"
#include "AOTLoaderWriter.h"
#include "DFGJITCompiler.h"
#include "LinkBuffer.h"
#include "AOTRelocTask.h"
#include "AOTDFGLinkInfo.h"
#include "DFGJITCode.h"
#include "CodeBlock.h"
#include "CodeBlockWithJITType.h"
#include "MacroAssemblerCodeRef.h"
#include "JSLexicalEnvironment.h"
#include "DFGSlowPathGenerator.h"
#include "DFGThunks.h"
#include "DFGJumpReplacement.h"
#include "DirectArguments.h"
#include "TypeProfilerLog.h"
#include "Watchdog.h"
#include "TypedArrayType.h"
#include "JSGeneratorFunction.h"
#include "AOTLoaderWriterInlines.h"
#if CPU(ARM64)
#include "ARM64Assembler.h"
#endif

namespace JSC {

AOTStoreBuffer::AOTStoreBuffer()
    : m_storage(AOTBUFFERSIZE)
    , m_buffer(m_storage.begin())
    , m_capacity(AOTBUFFERSIZE)
    , m_index(0)
{
}

void AOTStoreBuffer::grow(int extraCapacity)
{
    m_capacity += m_capacity + extraCapacity;
    m_storage.grow(m_capacity);
    m_buffer = m_storage.begin();
}

void AOTStoreBuffer::reset()
{
    m_storage.resize(AOTBUFFERSIZE);
    m_capacity = AOTBUFFERSIZE;
    m_index = 0;
    m_buffer = m_storage.begin();
}

template<typename IntegralType>
void AOTStoreBuffer::put(IntegralType value)
{
    ASSERT(isAvailable(sizeof(IntegralType)));
    *reinterpret_cast<IntegralType*>(m_buffer + m_index) = value;
    m_index += sizeof(IntegralType);
}

template<typename IntegralType>
void AOTStoreBuffer::put(IntegralType* data, int size)
{
    ensureSpace(sizeof(int) + size * sizeof(IntegralType));
    put(size);
    memcpy(m_buffer + m_index, data, size * sizeof(IntegralType));
    m_index += (size * sizeof(IntegralType));
}

template<typename IntegralType>
void AOTStoreBuffer::putVector(const Vector<IntegralType>& vector)
{
    int vectorSize = vector.size();
    ensureSpace(sizeof(int) + vectorSize * sizeof(IntegralType));

    put(vectorSize);
    memcpy(m_buffer + m_index, vector.data(), vectorSize * sizeof(IntegralType));
    m_index += (vectorSize * sizeof(IntegralType));
}

// AOTLoadBuffer
AOTLoadBuffer::AOTLoadBuffer()
    : m_storage(AOTBUFFERSIZE)
    , m_buffer(m_storage.begin())
    , m_index(0)
{
}

bool AOTLoadBuffer::getBool()
{
    return m_buffer[m_index++];
}

int AOTLoadBuffer::getInt()
{
    int value = *(reinterpret_cast<int*>(m_buffer + m_index));
    m_index += sizeof(int);
    return value;
}

unsigned AOTLoadBuffer::getUnsigned()
{
    unsigned value = *(reinterpret_cast<unsigned*>(m_buffer + m_index));
    m_index += sizeof(unsigned);
    return value;
}

JSValue AOTLoadBuffer::getJSValue()
{
    EncodedJSValue encode = *(reinterpret_cast<EncodedJSValue*>(m_buffer + m_index));
    m_index += sizeof(EncodedJSValue);

    JSValue value = JSValue::decode(encode);
    return value;
}

char* AOTLoadBuffer::getData(size_t size)
{
    char* point = m_buffer + m_index;
    m_index += size;

    return point;
}

void AOTLoadBuffer::getData(void* start, size_t size)
{
    memcpy(start, m_buffer + m_index, size);
    m_index += size;
}

template<typename IntegralType>
void AOTLoadBuffer::getVector(Vector<IntegralType>& vector)
{
    int length = getInt();
    char* data = m_buffer + m_index;
    m_index += (length * sizeof(IntegralType));

    vector.resize(length);
    memcpy(reinterpret_cast<char*>(vector.data()), data, length * sizeof(IntegralType));
}


AOTWriter::AOTWriter(VM* vm, AOT* aot)
    : m_vm(vm)
    , m_aot(aot)
{
}

AOTWriter::~AOTWriter()
{
}

void AOTWriter::writeDFGJITCode(DFG::JITCompiler& jit)
{
    ASSERT(!bufferSize());

    m_buffer.put(jit.m_assembler.buffer().data(), jit.m_assembler.buffer().codeSize());
}

bool AOTWriter::writeDFGJITInfo(DFG::JITCompiler& jit, bool isGlobalCode)
{
    // CodeBlock Info
    writeDFGCodeBlockInfo(jit.codeBlock());

    // AOTRelocTask
    Vector<AOTRelocTask>& relocTasks = jit.m_assembler.buffer().tasks();
    for (unsigned i = 0; i < relocTasks.size(); i++) {
        AOTRelocTask& task = relocTasks[i];
        if (task.type == AOTRelocTask::StringImpl) {
            StringImpl* impl = reinterpret_cast<StringImpl*>(task.attr);
            if (LIKELY(impl->isAtomic())) {
                if (!m_stringTable->validate(impl))
                    return false;
                task.attr = m_stringTable->add(impl);
            } else {
                ASSERT(impl->isSymbol());
                ASSERT(impl->is8Bit());
                ASSERT(impl->length());
                task.type = AOTRelocTask::SymbolStringImpl;
                task.attr = m_vm->propertyNames->builtinNames().findSymbolIndex(impl); 
            }
        }
    }
    m_buffer.putVector(relocTasks);

    // m_blockHeads
    m_buffer.putVector(jit.m_blockHeads);


    // link

    Vector<DFG::SwitchData*> switchData;
    for (Bag<DFG::SwitchData>::iterator iter = jit.m_graph.m_switchData.begin(); !!iter; ++iter) {
        DFG::SwitchData* data = *iter;
        if (!data->didUseJumpTable)
            continue;
        switchData.append(data);
    }
    
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(switchData.size());
    for (unsigned i = switchData.size(); i--;) {
        DFG::SwitchData& data = *switchData[i];

        ASSERT(data.kind != DFG::SwitchKind::SwitchCell);

        m_buffer.ensureSpace(sizeof(AOTSwitchData));
        m_buffer.put(AOTSwitchData(data));

        Vector<AOTSwitchCase> switchCases;
        for (unsigned j = data.cases.size(); j--;) {
            DFG::SwitchCase myCase = data.cases[j];
            unsigned lookup;

            if (data.kind == DFG::SwitchKind::SwitchString) { 
                if (!m_stringTable->validate(myCase.value.stringImpl()))
                    return false;
                lookup = m_stringTable->add(myCase.value.stringImpl()); 
            } else { 
                lookup = myCase.value.switchLookupValue(data.kind);
            }

            switchCases.append(AOTSwitchCase(lookup, myCase.target.block->index));
        }
        m_buffer.putVector(switchCases);
    }

    // m_calls
    Vector<DFG::CallLinkRecord> calls(jit.m_calls);
    for (unsigned i = 0; i < calls.size(); i++) {
        unsigned index = m_aot->getFunctionIndex(calls[i].m_function.value());
        calls[i].m_function.setValue(reinterpret_cast<void*>(index));
    }
    m_buffer.putVector(calls);

    // m_getByIds
    Vector<AOTByIdRecord> byIdRecord;
    for (unsigned i = 0; i < jit.m_getByIds.size(); i++) {
        JITByIdGenerator& generator = jit.m_getByIds[i].m_generator; 
        DFG::SlowPathGenerator* slowPath = jit.m_getByIds[i].m_slowPath;
        byIdRecord.append(AOTByIdRecord(generator.m_structureImm, generator.m_structureCheck, generator.m_loadOrStore, generator.m_tagLoadOrStore, generator.m_done, slowPath->label(), slowPath->call(), generator.m_stubInfo->indexInCodeBlock));
    }

    // m_putByIds
    for (unsigned i = 0; i < jit.m_putByIds.size(); i++) {
        JITByIdGenerator& generator = jit.m_putByIds[i].m_generator; 
        DFG::SlowPathGenerator* slowPath = jit.m_putByIds[i].m_slowPath;
        byIdRecord.append(AOTByIdRecord(generator.m_structureImm, generator.m_structureCheck, generator.m_loadOrStore, generator.m_tagLoadOrStore, generator.m_done, slowPath->label(), slowPath->call(), generator.m_stubInfo->indexInCodeBlock));
    }
    m_buffer.putVector(byIdRecord);

    // m_ins
    Vector<AOTInRecord> inRecord;
    for (unsigned i = 0; i < jit.m_ins.size(); i++) {
        DFG::InRecord& record = jit.m_ins[i];
        inRecord.append(AOTInRecord(record.m_jump, record.m_done, record.m_slowPathGenerator->call(), record.m_slowPathGenerator->label(), record.m_stubInfo->indexInCodeBlock));
    }
    m_buffer.putVector(inRecord);

    // m_jsCalls
    Vector<AOTJSCallRecord> jsCallRecord;
    for (unsigned i = 0; i < jit.m_jsCalls.size(); i++) {
        DFG::JITCompiler::JSCallRecord& record = jit.m_jsCalls[i];
        jsCallRecord.append(AOTJSCallRecord(record.m_fastCall, record.m_slowCall, record.m_targetToCheck, record.m_info->m_indexInCodeBlock));
    }
    m_buffer.putVector(jsCallRecord);

    // JITCode  data
    writeDFGJITCodeInfo(jit);

    // Link after DFGJITCompiler::link
    // m_callArityFixup and m_arityCheck
    if (!isGlobalCode) {
        m_buffer.ensureSpace(sizeof(Call) + sizeof(Label));
        m_buffer.put(jit.m_callArityFixup);
        m_buffer.put(jit.m_arityCheck);
    }

    return true;
}

void AOTWriter::writeDFGCodeBlockInfo(CodeBlock* codeBlock)
{
    // m_stubInfos
    Vector<StructureStubInfo> stubInfos;
    for (Bag<StructureStubInfo>::iterator iter = codeBlock->stubInfoBegin(); !!iter; ++iter) {
        StructureStubInfo* stub = *iter;
        stubInfos.append(*stub);
    }
    m_buffer.putVector(stubInfos);

    // m_callLinkInfos
    Vector<CallLinkInfo> callLinkInfos;
    for (Bag<CallLinkInfo>::iterator iter = codeBlock->callLinkInfosBegin(); !!iter; ++iter) {
        CallLinkInfo* callInfo = *iter;
        callLinkInfos.append(*callInfo);
    }
    m_buffer.putVector(callLinkInfos);
}

void AOTWriter::writeDFGJITCodeInfo(DFG::JITCompiler& jit)
{
    // m_jitCode->osrExit
    ASSERT(jit.m_jitCode->osrExit.size() == jit.m_exitCompilationInfo.size());
    Vector<DFG::OSRExit> osrExit;
    Vector<AOTOSRExitCompilationInfo> exitCompilationInfo;
    for (unsigned i = 0; i < jit.m_jitCode->osrExit.size(); i++) {
        DFG::OSRExit& exit = jit.m_jitCode->osrExit[i];
        DFG::OSRExitCompilationInfo& info = jit.m_exitCompilationInfo[i];
        osrExit.append(exit);
        exitCompilationInfo.append(AOTOSRExitCompilationInfo(info));
    }
    m_buffer.putVector(osrExit);
    m_buffer.putVector(exitCompilationInfo);

    // JITCode data
    m_buffer.ensureSpace(sizeof(unsigned) * 2);
    m_buffer.put(jit.m_graph.frameRegisterCount());
    m_buffer.put(jit.m_graph.requiredRegisterCountForExit());
    m_buffer.putVector(jit.m_jitCode->speculationRecovery);
    m_buffer.putVector(jit.m_jitCode->variableEventStream);

    Vector<DFG::MinifiedNode>& minifiedNodes = jit.m_jitCode->minifiedDFG.m_list;
    Vector<AOTMinifiedNodeInfo> minifiedNodeInfos;
    for (unsigned i = 0; i < minifiedNodes.size(); i++) {
        DFG::MinifiedNode& node = minifiedNodes[i];
        if (node.hasConstantCell()) {
            AOTRelocTask task = node.getRelocTask(jit.m_graph);
            ASSERT(task.isValid());
            minifiedNodeInfos.append(AOTMinifiedNodeInfo(i, task));
        }
    } 
    m_buffer.putVector(minifiedNodes);
    m_buffer.putVector(minifiedNodeInfos);
}

AOTLoader::AOTLoader(VM* vm, AOT* aot)
    : m_vm(vm)
    , m_aot(aot)
{
}

AOTLoader::~AOTLoader()
{
}

void AOTLoader::loadDataFile(FILE* file, int size)
{
    m_buffer.resize(size);
    fread(m_buffer.data(), sizeof(char), size, file);
}

void AOTLoader::loadDFGJITCode(LinkBuffer& linkBuffer, CodeBlock* codeBlock)
{
    int size = m_buffer.getInt();
    linkBuffer.linkCode(codeBlock, m_buffer.getData(size), size);
}

void AOTLoader::loadDFGJITInfo(LinkBuffer& linkBuffer, CodeBlock* codeBlock, bool isGlobalCode)
{
    // CodeBlock
    loadDFGCodeBlockInfo(codeBlock);

    // AOTRelocTask
    relocateAOTRelocTasks(linkBuffer, codeBlock);

    Vector<Label> blockHeads;
    m_buffer.getVector(blockHeads);

    // Switch
    BitVector usedJumpTables;
    unsigned numOfSwitchData = m_buffer.getUnsigned();
    for (unsigned i = 0; i < numOfSwitchData; ++i) {
        AOTSwitchData data;
        m_buffer.getData(&data, sizeof(AOTSwitchData));

        ASSERT(data.didUseJumpTable);
        
        if (data.kind == DFG::SwitchKind::SwitchString) {
            StringJumpTable& table = codeBlock->stringSwitchJumpTable(data.switchTableIndex);
            table.ctiDefault = linkBuffer.locationOf(blockHeads[data.fallThroughIndex]);
            StringJumpTable::StringOffsetTable::iterator iter;
            StringJumpTable::StringOffsetTable::iterator end = table.offsetTable.end();
            for (iter = table.offsetTable.begin(); iter != end; ++iter)
                iter->value.ctiOffset = table.ctiDefault;

            Vector<AOTSwitchCase> cases;
            m_buffer.getVector(cases);
            for (unsigned j = cases.size(); j--;) {
                AOTSwitchCase& myCase = cases[j];
                iter = table.offsetTable.find(m_stringTable->get(myCase.switchLookupValue));
                ASSERT(iter != end);
                iter->value.ctiOffset = linkBuffer.locationOf(blockHeads[myCase.targetIndex]);
            }
        } else {
            ASSERT(data.kind == DFG::SwitchKind::SwitchImm || data.kind == DFG::SwitchKind::SwitchChar);
        
            usedJumpTables.set(data.switchTableIndex);
            SimpleJumpTable& table = codeBlock->switchJumpTable(data.switchTableIndex);
            table.ctiDefault = linkBuffer.locationOf(blockHeads[data.fallThroughIndex]);
            table.ctiOffsets.grow(table.branchOffsets.size());
    
            for (unsigned j = table.ctiOffsets.size(); j--;)
                table.ctiOffsets[j] = table.ctiDefault;

            Vector<AOTSwitchCase> cases;
            m_buffer.getVector(cases);
            for (unsigned j = cases.size(); j--;) {
                AOTSwitchCase& myCase = cases[j];
                table.ctiOffsets[myCase.switchLookupValue - table.min] =
                    linkBuffer.locationOf(blockHeads[myCase.targetIndex]);
            }
        }
    }
    
    for (unsigned i = codeBlock->numberOfSwitchJumpTables(); i--;) {
        if (usedJumpTables.get(i))
            continue;
        codeBlock->switchJumpTable(i).clear();
    }


    // m_calls
    Vector<DFG::CallLinkRecord> calls;
    m_buffer.getVector(calls);
    for (unsigned i = 0; i < calls.size(); ++i) {
        void* funcPtr = m_aot->getFunctionPtr(reinterpret_cast<unsigned>(calls[i].m_function.value()));
        calls[i].m_function.setValue(funcPtr);
        
        linkBuffer.link(calls[i].m_call, calls[i].m_function);
    }

    // m_getByIds, m_putByIds
    Vector<AOTByIdRecord> byIdRecord;;
    m_buffer.getVector(byIdRecord);
    for (unsigned i = byIdRecord.size(); i--;) {
        StructureStubInfo* stubInfo = codeBlock->findStubInfo(byIdRecord[i].m_stubInfoIndex);
        CodeLocationCall callReturnLocation = linkBuffer.locationOf(byIdRecord[i].m_call);

        stubInfo->callReturnLocation = callReturnLocation;
        stubInfo->patch.deltaCheckImmToCall = MacroAssembler::differenceBetweenCodePtr(
            linkBuffer.locationOf(byIdRecord[i].m_structureImm), callReturnLocation);
        stubInfo->patch.deltaCallToJump = MacroAssembler::differenceBetweenCodePtr(
            callReturnLocation, linkBuffer.locationOf(byIdRecord[i].m_structureCheck));
        stubInfo->patch.deltaCallToTagLoadOrStore = MacroAssembler::differenceBetweenCodePtr(
            callReturnLocation, linkBuffer.locationOf(byIdRecord[i].m_tagLoadOrStore));
        stubInfo->patch.deltaCallToPayloadLoadOrStore = MacroAssembler::differenceBetweenCodePtr(
            callReturnLocation, linkBuffer.locationOf(byIdRecord[i].m_loadOrStore));
        stubInfo->patch.deltaCallToSlowCase = MacroAssembler::differenceBetweenCodePtr(
            callReturnLocation, linkBuffer.locationOf(byIdRecord[i].m_slowPathBegin));
        stubInfo->patch.deltaCallToDone = MacroAssembler::differenceBetweenCodePtr(
            callReturnLocation, linkBuffer.locationOf(byIdRecord[i].m_done));
    }

    // m_ins
    Vector<AOTInRecord> inRecord;
    m_buffer.getVector(inRecord);
    for (unsigned i = 0; i < inRecord.size(); ++i) {
        AOTInRecord& record = inRecord[i];
        StructureStubInfo& info = *codeBlock->findStubInfo(record.m_stubInfoIndex);
        CodeLocationCall callReturnLocation = linkBuffer.locationOf(record.m_slowPathCall);
        info.patch.deltaCallToDone = MacroAssembler::differenceBetweenCodePtr(callReturnLocation, linkBuffer.locationOf(record.m_done));
        info.patch.deltaCallToJump = MacroAssembler::differenceBetweenCodePtr(callReturnLocation, linkBuffer.locationOf(record.m_jump));
        info.callReturnLocation = callReturnLocation;
        info.patch.deltaCallToSlowCase = MacroAssembler::differenceBetweenCodePtr(callReturnLocation, linkBuffer.locationOf(record.m_slowPathLabel));
    }

    // m_jsCalls
    Vector<AOTJSCallRecord> jsCallRecord;
    m_buffer.getVector(jsCallRecord);
    for (unsigned i = 0; i < jsCallRecord.size(); ++i) {
        AOTJSCallRecord& record = jsCallRecord[i];
        CallLinkInfo& info = *codeBlock->findCallLinkInfo(record.m_callLinkInfoIndex);
        linkBuffer.link(record.m_slowCall, FunctionPtr(m_vm->getCTIStub(linkCallThunkGenerator).code().executableAddress()));
        info.setCallLocations(linkBuffer.locationOfNearCall(record.m_slowCall),
            linkBuffer.locationOf(record.m_targetToCheck),
            linkBuffer.locationOfNearCall(record.m_fastCall));
    }

    // JITCode data
    loadDFGJITCodeInfo(linkBuffer, codeBlock);

    // Link after DFGJITCompiler::link
    if (!isGlobalCode) {
        Call callArityFixup;
        Label arityCheck;
        m_buffer.getData(&callArityFixup, sizeof(Call));
        m_buffer.getData(&arityCheck, sizeof(Label));
        linkBuffer.link(callArityFixup, FunctionPtr((m_vm->getCTIStub(arityFixupGenerator)).code().executableAddress()));
        m_withArityCheck = linkBuffer.locationOf(arityCheck);
    }
}

void AOTLoader::loadDFGCodeBlockInfo(CodeBlock* codeBlock)
{
    codeBlock->setCalleeSaveRegisters(RegisterSet::dfgCalleeSaveRegisters());

    Vector<StructureStubInfo> stubInfos;
    Vector<CallLinkInfo> callLinkInfos;

    m_buffer.getVector(stubInfos);
    m_buffer.getVector(callLinkInfos);

    for (unsigned i = stubInfos.size(); i--;)
        codeBlock->addStubInfo(stubInfos[i]);
    for (unsigned i = callLinkInfos.size(); i--;)
        codeBlock->addCallLinkInfo(callLinkInfos[i]);
}

void AOTLoader::loadDFGJITCodeInfo(LinkBuffer& linkBuffer, CodeBlock* codeBlock)
{
    Vector<AOTOSRExitCompilationInfo> exitCompilationInfo;
    ASSERT(m_osrExit.isEmpty());
    m_buffer.getVector(m_osrExit);
    m_buffer.getVector(exitCompilationInfo);
    ASSERT(m_osrExit.size() == exitCompilationInfo.size());

    // m_jiktCode->osrExit;
    ASSERT(m_jumpReplacements.isEmpty());
    MacroAssemblerCodeRef osrExitThunk = m_vm->getCTIStub(DFG::osrExitGenerationThunkGenerator);
    CodeLocationLabel target = CodeLocationLabel(osrExitThunk.code());
    CodeBlock* profiledBlock = codeBlock->baselineAlternative();
    for (unsigned i = 0; i < m_osrExit.size(); ++i) {
        DFG::OSRExit& exit = m_osrExit[i];
        AOTOSRExitCompilationInfo& info = exitCompilationInfo[i];
        MethodOfGettingAValueProfile& profile = exit.m_valueProfile;
    
        if (profile.m_kind == MethodOfGettingAValueProfile::Kind::Ready) {
            int profileIndex = profile.valueProfileIndex;
            ASSERT(profileIndex >= 0);
            profile.u.profile = profile.isArgument ? profiledBlock->valueProfileForArgument(profileIndex) : profiledBlock->valueProfileForBytecodeOffset(profileIndex);
        } else if (profile.m_kind == MethodOfGettingAValueProfile::Kind::LazyOperand) {
            profile.u.lazyOperand.codeBlock = profiledBlock;
        }

        linkBuffer.link(exit.getPatchableCodeOffsetAsJump(), target);
        exit.correctJump(linkBuffer);
        if (info.m_replacementSource.isSet()) {
            m_jumpReplacements.append(DFG::JumpReplacement(
                linkBuffer.locationOf(info.m_replacementSource),
                linkBuffer.locationOf(info.m_replacementDestination)));
        }
    }

    // JITCode data
    Vector<AOTMinifiedNodeInfo> minifiedNodeInfos;
    m_frameRegisterCount = m_buffer.getUnsigned();
    m_requiredRegisterCountForExit = m_buffer.getUnsigned();
    ASSERT(m_speculationRecovery.isEmpty() && m_variableEvents.isEmpty() && m_minifiedNodes.isEmpty());
    m_buffer.getVector(m_speculationRecovery);
    m_buffer.getVector(m_variableEvents);
    m_buffer.getVector(m_minifiedNodes);
    m_buffer.getVector(minifiedNodeInfos);

    for (unsigned i = 0; i < minifiedNodeInfos.size(); i++) {
        unsigned index = minifiedNodeInfos[i].m_index;
        AOTRelocTask task = minifiedNodeInfos[i].m_task;
        JSCell* value;

        switch (task.type) {
        case AOTRelocTask::GlobalObject: {
            value = codeBlock->globalObject();
            break;
        }
        case AOTRelocTask::GlobalLexicalEnvironment: {
            value = codeBlock->globalObject()->globalLexicalEnvironment();
            break;
        }
        case AOTRelocTask::ConstantScope: {
            value = jsCast<JSSegmentedVariableObject*>(JSScope::constantScopeForCodeBlock(static_cast<ResolveType>(task.attr), codeBlock));
            break;
        }
        case AOTRelocTask::ConstantCell: {
            value = codeBlock->getConstant(task.attr).asCell();
            break;
        }
        default: {
            RELEASE_ASSERT_NOT_REACHED();
            break;
        }
        }

        m_minifiedNodes[index].setConstantCell(value);
    }
}

void AOTLoader::relocateAOTRelocTasks(LinkBuffer& linkBuffer, CodeBlock* codeBlock)
{
    static const double zero = 0;
    static const double one = 1.0;
    static const double half = 0.5;
    static const double byteMax = 255;
    static const double NaN = PNaN;
    static const double halfConstant = 0.5;
    static const double oneConstant = -1.0;
    static const double negativeZeroConstant = -0.0;

    ScratchBuffer* scratchBuffer = nullptr;
    EncodedJSValue* buffer = nullptr;

    Vector<AOTRelocTask> relocTasks;
    m_buffer.getVector(relocTasks);

    intptr_t new_address;
    AOTRelocTask task;
    Instruction* instructionBegin = codeBlock->instructions().begin();
    Instruction* currentInstruction; 
    for (int i=0; i<relocTasks.size(); i++) {
        task = relocTasks[i];
        currentInstruction = instructionBegin + task.bc;

        switch (task.type) {
        case AOTRelocTask::FuncDecl: {
            FunctionExecutable* executable = codeBlock->functionDecl(task.attr);
            new_address = reinterpret_cast<intptr_t>(executable);
            break;
        }
        case AOTRelocTask::FuncExpr: {
            FunctionExecutable* executable = codeBlock->functionExpr(task.attr);
            new_address = reinterpret_cast<intptr_t>(executable);
            break;
        }
        case AOTRelocTask::CodeBlock: { 
            new_address = reinterpret_cast<intptr_t>(codeBlock);
            break;
        }
        case AOTRelocTask::StackLimit: { 
            new_address = reinterpret_cast<intptr_t>(m_vm->addressOfStackLimit());
            break;
        }
        case AOTRelocTask::TopCallFrame: {
            new_address = reinterpret_cast<intptr_t>((&m_vm->topCallFrame) + task.attr * 4);
            break;
        }
        case AOTRelocTask::Exception: {
            new_address = reinterpret_cast<intptr_t>(m_vm->addressOfException());
            break;
        }
        case AOTRelocTask::SymTable: {
            JSValue val = codeBlock->getConstant(task.attr);
            new_address = reinterpret_cast<intptr_t>(val.asCell());
            break;
        }
        case AOTRelocTask::Pointer: {
            int32_t* pointer = &reinterpret_cast<EncodedValueDescriptor*>(currentInstruction[task.attr].u.pointer)->asBits.payload;
            new_address = reinterpret_cast<intptr_t>(pointer);
            break;
        }
        case AOTRelocTask::PointerTag: {
            int32_t* pointer = &reinterpret_cast<EncodedValueDescriptor*>(currentInstruction[task.attr].u.pointer)->asBits.tag;
            new_address = reinterpret_cast<intptr_t>(pointer);
            break;
        }
        case AOTRelocTask::SpecialPointer:
            new_address = reinterpret_cast<intptr_t>(actualPointerFor(codeBlock, currentInstruction[task.attr].u.specialPointer));
            break;
        case AOTRelocTask::Regexp: {
            RegExp* regexp = codeBlock->regexp(task.attr);
            new_address = reinterpret_cast<intptr_t>(regexp);
            break;
        }
        case AOTRelocTask::CopiedAllocatorRemaining: {
            CopiedAllocator* allocator = &m_vm->heap.storageAllocator();
            new_address = reinterpret_cast<intptr_t>(&allocator->m_currentRemaining);
            break;
        }
        case AOTRelocTask::CopiedAllocatorPayloadEnd: {
            CopiedAllocator* allocator = &m_vm->heap.storageAllocator();
            new_address = reinterpret_cast<intptr_t>(&allocator->m_currentPayloadEnd);
            break;
        }
        case AOTRelocTask::FuncAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<JSFunction>(JSFunction::allocationSize(0));
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::GenFuncAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<JSGeneratorFunction>(JSGeneratorFunction::allocationSize(0));
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::ObjAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectWithoutDestructor(task.attr);
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::LEAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<JSLexicalEnvironment>(task.attr);
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::JSRopeAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectWithDestructor(task.attr);
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::DAAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<DirectArguments>(task.attr);
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::StringObjAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<StringObject>(StringObject::allocationSize(0));
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::JSArrayBVAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<JSArrayBufferView>(JSArrayBufferView::allocationSize(0));
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::JSArrayAllocator: {
            MarkedAllocator* allocator = &m_vm->heap.allocatorForObjectOfType<JSArray>(JSArray::allocationSize(0));
            new_address = reinterpret_cast<intptr_t>(allocator);
            break;
        }
        case AOTRelocTask::DASpace: {
            //MarkedSpace::Subspace& subspace = m_vm->heap.subspaceForObjectOfType<DirectArguments>();
            static MarkedSpace::Subspace* subspace = nullptr;
            if (task.attr) {
                subspace = &m_vm->heap.subspaceForObjectOfType<DirectArguments>();
                new_address = reinterpret_cast<intptr_t>(&subspace->preciseAllocators[0]);
            }
            else {
                new_address = reinterpret_cast<intptr_t>(&subspace->impreciseAllocators[0]);
                subspace = nullptr;
            }
            break;
        }
        case AOTRelocTask::FuncStructure: {
            Structure* structure = codeBlock->globalObject()->functionStructure();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::GenFuncStructure: {
            Structure* structure = codeBlock->globalObject()->generatorFunctionStructure();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::ObjAllocStructure: {
            Structure* structure = currentInstruction[task.attr].u.objectAllocationProfile->structure();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::LEStructure: {
            Structure* structure = codeBlock->globalObject()->activationStructure();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::StrStructure: {
            Structure* structure = m_vm->stringStructure.get();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::DAStructure: {
            Structure* structure = codeBlock->globalObject()->directArgumentsStructure();
            new_address = reinterpret_cast<intptr_t>(structure);
            break;
        }
        case AOTRelocTask::ProtoHasInstanceSymbolFunction: {
            JSFunction* function = codeBlock->globalObject()->functionProtoHasInstanceSymbolFunction();
            new_address = reinterpret_cast<intptr_t>(function);
            break;
        }
        case AOTRelocTask::ConstantCell: {
            JSValue value = codeBlock->getConstant(task.attr);
            new_address = reinterpret_cast<intptr_t>(value.asCell());
            break;
        }
        case AOTRelocTask::ConstantScope: {
            JSSegmentedVariableObject* scopeObject = jsCast<JSSegmentedVariableObject*>(JSScope::constantScopeForCodeBlock(static_cast<ResolveType>(task.attr), codeBlock));
            new_address = reinterpret_cast<intptr_t>(scopeObject);
            break;
        }
        case AOTRelocTask::GlobalObject: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject());
            break;
        }
        case AOTRelocTask::GlobalLexicalEnvironment: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject()->globalLexicalEnvironment());
            break;
        }
        case AOTRelocTask::ArrayStructure: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject()->arrayStructureForIndexingTypeDuringAllocation(task.attr));
            break;
        }
        case AOTRelocTask::StringObjectStructure: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject()->stringObjectStructure());
            break;
        }
        case AOTRelocTask::TypedArrayStructure: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject()->typedArrayStructure(static_cast<TypedArrayType>(task.attr)));
            break;
        }
        case AOTRelocTask::CreateScratchBuffer: {
            ASSERT(!buffer);
            ASSERT(!scratchBuffer);
            scratchBuffer = m_vm->scratchBufferForSize(task.attr);
            buffer = scratchBuffer ? static_cast<EncodedJSValue*>(scratchBuffer->dataBuffer()) : 0;
            continue;
        }
        case AOTRelocTask::ScratchBuffer: {
            ASSERT(buffer);
            new_address = reinterpret_cast<intptr_t>(buffer);
            break;
        }
        case AOTRelocTask::ScratchBufferIdx: {
            ASSERT(buffer);
            new_address = reinterpret_cast<intptr_t>(buffer + task.attr);
            break;
        }
        case AOTRelocTask::ScratchBufferIdxTag: {
            ASSERT(buffer);
            new_address = reinterpret_cast<intptr_t>(buffer + task.attr) + OBJECT_OFFSETOF(EncodedValueDescriptor, asBits.tag);
            break;
        }
        case AOTRelocTask::ScratchBufferIdxPayload: {
            ASSERT(buffer);
            new_address = reinterpret_cast<intptr_t>(buffer + task.attr) + OBJECT_OFFSETOF(EncodedValueDescriptor, asBits.payload);
            break;
        }
        case AOTRelocTask::ScratchBufferALP: {
            ASSERT(scratchBuffer);
            new_address = reinterpret_cast<intptr_t>(scratchBuffer->activeLengthPtr());
            break;
        }
        case AOTRelocTask::EndOfScratchBuffer: {
            ASSERT(buffer);
            ASSERT(scratchBuffer);
            scratchBuffer = nullptr;
            buffer = nullptr;
            continue;
        }
        case AOTRelocTask::TypeProfilerLog: {
            new_address = reinterpret_cast<intptr_t>(m_vm->typeProfilerLog());
            break;
        }
        case AOTRelocTask::TypeProfilerLogLEP: {
            new_address = reinterpret_cast<intptr_t>(m_vm->typeProfilerLog()->logEndPtr());
            break;
        }
        case AOTRelocTask::WatchDogDFA: {
            new_address = reinterpret_cast<intptr_t>(m_vm->watchdog()->timerDidFireAddress());
            break;
        }
        case AOTRelocTask::SingleCharStr: {
            new_address = reinterpret_cast<intptr_t>(m_vm->smallStrings.singleCharacterStrings());
            break;
        }
        case AOTRelocTask::TwoToThe32: {
            new_address = reinterpret_cast<intptr_t>(&AssemblyHelpers::twoToThe32);
            break;
        }
        case AOTRelocTask::Num: {
            switch (task.attr) {
            case 0 : {
                new_address = reinterpret_cast<intptr_t>(&zero);
                break;
            }
            case 1 : {
                new_address = reinterpret_cast<intptr_t>(&one);
                break;
            }
            case 2 : {
                new_address = reinterpret_cast<intptr_t>(&half);
                break;
            }
            case 3 : {
                new_address = reinterpret_cast<intptr_t>(&byteMax);
                break;
            }
            case 4 : {
                new_address = reinterpret_cast<intptr_t>(&NaN);
                break;
            }
            case 5 : {
                new_address = reinterpret_cast<intptr_t>(&halfConstant);
                break;
            }
            case 6 : {
                new_address = reinterpret_cast<intptr_t>(&oneConstant);
                break;
            }
            case 7 : {
                new_address = reinterpret_cast<intptr_t>(&negativeZeroConstant);
                break;
            }
            default : {
                RELEASE_ASSERT_NOT_REACHED();
            }
            }
            break;
        }
        case AOTRelocTask::WriteBarrierBuffer: {
            new_address = reinterpret_cast<intptr_t>(m_vm->heap.writeBarrierBuffer().buffer());
            break;
        }
        case AOTRelocTask::OSRExitIndex: {
            new_address = reinterpret_cast<intptr_t>(&m_vm->osrExitIndex);
            break;
        }
        case AOTRelocTask::StringObjectInfo: {
            new_address = reinterpret_cast<intptr_t>(StringObject::info());
            break;
        }
        case AOTRelocTask::SwitchJumpTable: {
            SimpleJumpTable& table = codeBlock->switchJumpTable(task.attr);
            table.ensureCTITable();
            new_address = reinterpret_cast<intptr_t>(table.ctiOffsets.begin());
            break;
        }
        case AOTRelocTask::CallLinkInfo: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->findCallLinkInfo(task.attr));
            break;
        }
        case AOTRelocTask::StubInfo: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->findStubInfo(task.attr));
            break;
        }
        case AOTRelocTask::StringImpl: {
            new_address = reinterpret_cast<intptr_t>(m_stringTable->get(task.attr));
            break;
        }
        case AOTRelocTask::SymbolStringImpl: {
            new_address = reinterpret_cast<intptr_t>(m_vm->propertyNames->builtinNames().getSymbol(task.attr));
            break;
        }
        case AOTRelocTask::ToThis: {
            new_address = reinterpret_cast<intptr_t>(codeBlock->globalObject()->globalThis());
            break;
        }
        case AOTRelocTask::DoubleConstant: {
            new_address = reinterpret_cast<intptr_t>(addressOfDoubleConstant(task.doubleValue));
            break;
        }
        case AOTRelocTask::VM: {
            new_address = reinterpret_cast<intptr_t>(m_vm);
            break;
        }
        case AOTRelocTask::PCForThrow: {
            new_address = reinterpret_cast<intptr_t>(&m_vm->targetMachinePCForThrow);
            break;
        }
        case AOTRelocTask::Null:
        case AOTRelocTask::Local:
        default:
            RELEASE_ASSERT_NOT_REACHED();
            break;
        }
        // fixup
        ASSERT(task.mc);
#if CPU(X86)
        linkBuffer.patch(reinterpret_cast<intptr_t>(new_address), task.mc);
        //relocator.assembler().buffer().patch(reinterpret_cast<intptr_t>(new_address), mc);
#elif CPU(ARM64)
        //const int dataSize = 64;
        const int numberHalfWords = 4;
        uint16_t halfword[numberHalfWords];

        int zeroOrNegateVote = 0;
        for (int i = 0; i < numberHalfWords; ++i) {
            halfword[i] = getHalfword(new_address, i);
            if (!halfword[i])
                zeroOrNegateVote++;
            else if (halfword[i] == 0xffff)
                zeroOrNegateVote--;
        }
        bool needToClearRegister = true;
        if (zeroOrNegateVote >= 0) {
            for (int i = 0; i < numberHalfWords; i++) {
                if (halfword[i]) {
                    if (needToClearRegister) {
                        linkBuffer.patch16(halfword[i], i);
                        needToClearRegister = false;
                    } else
                        linkBuffer.patch16(halfword[i], i);
                }
            }
        } else {
            for (int i = 0; i < numberHalfWords; i++) {
                if (halfword[i] != 0xffff) {
                    if (needToClearRegister) {
                        linkBuffer.patch16(~halfword[i], i);
                        needToClearRegister = false;
                    } else
                        linkBuffer.patch16(halfword[i], i);
                }
            }
        }
#endif
    }
}

void AOTLoader::setJITCodeInfo(DFG::JITCode* jitCode)
{
    // jitCode data
    jitCode->speculationRecovery = m_speculationRecovery;
    for (unsigned i = 0; i < m_variableEvents.size(); i++)
        jitCode->variableEventStream.appendAndLog(m_variableEvents[i]);
    jitCode->minifiedDFG.m_list = m_minifiedNodes;

    // jitCode common data 
    jitCode->common.frameRegisterCount = m_frameRegisterCount;
    jitCode->common.requiredRegisterCountForExit = m_requiredRegisterCountForExit;
    jitCode->common.jumpReplacements = m_jumpReplacements;
#if USE(JSVALUE32_64)
    jitCode->common.doubleConstants = WTFMove(m_doubleConstants);
    m_doubleConstantsMap.clear();
#endif

    // jitCode data
    for (unsigned i = 0; i < m_osrExit.size(); i++) 
        jitCode->osrExit.append(m_osrExit[i]);

    m_speculationRecovery.clear();
    m_variableEvents.clear();
    m_minifiedNodes.clear();
    m_jumpReplacements.clear();
    m_osrExit.clear();
}

void AOTLoader::finalize(LinkBuffer& linkBuffer, CodeBlock* codeBlock)
{
    RefPtr<DFG::JITCode> jitCode = adoptRef(new DFG::JITCode());

    setJITCodeInfo(jitCode.get());

    jitCode->initializeCodeRef(
        FINALIZE_DFG_CODE(linkBuffer, ("DFG JIT code for %s", toCString(CodeBlockWithJITType(codeBlock, JITCode::DFGJIT)).data())),
        MacroAssemblerCodePtr());
    codeBlock->setJITCode(jitCode);
}

void AOTLoader::finalizeFunction(LinkBuffer& linkBuffer, CodeBlock* codeBlock)
{
    ASSERT(!m_withArityCheck.isEmptyValue());

    RefPtr<DFG::JITCode> jitCode = adoptRef(new DFG::JITCode());

    setJITCodeInfo(jitCode.get());

    jitCode->initializeCodeRef(
        FINALIZE_DFG_CODE(linkBuffer, ("DFG JIT code for %s", toCString(CodeBlockWithJITType(codeBlock, JITCode::DFGJIT)).data())),
        m_withArityCheck);
    codeBlock->setJITCode(jitCode);
}

}

#endif
/* END DFG_AOT */
