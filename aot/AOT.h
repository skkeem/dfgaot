#ifndef AOT_h
#define AOT_h

#if ENABLE(DFG_AOT) || ENABLE(BYTE_AOT) 

#include "AOTKey.h"
#include <wtf/RefPtr.h>
#include <wtf/RefCounted.h>
#include <wtf/Vector.h>

namespace JSC {

    class VM;
    class CodeBlock;
    class UnlinkedCodeBlock;
    class UnlinkedFunctionCodeBlock;
    class UnlinkedProgramCodeBlock;
    class UnlinkedFunctionExecutable;
    class ProgramExecutable;
    class SourceProvider;
    class AOTWriter;
    class AOTLoader;
    class AOTStringTable;

    namespace DFG {
        class JITCompiler;
    }

    class AOT : public RefCounted<AOT> {
    private:
        static const char* filePathPrefix;
        static const char* byteMetaFileSuffix;
        static const char* dfgMetaFileSuffix;
        static const char* byteDataFileSuffix;
        static const char* dfgDataFileSuffix;
        static const char* stringFileSuffix;

        struct AOTFileName {
            AOTFileName() { }

            /*
            AOTFileName(const AOTFileName& other)
            {
                ASSERT(strlen(other.dfgMetaFileName));
                ASSERT(strlen(other.dfgDataFileName));
                ASSERT(strlen(other.stringFileName));
                strcpy(byteMetaFileName, other.byteMetaFileName);
                strcpy(dfgMetaFileName, other.dfgMetaFileName);
                strcpy(byteDataFileName, other.byteDataFileName);
                strcpy(dfgDataFileName, other.dfgDataFileName);
                strcpy(stringFileName, other.stringFileName);
            }
            */

            char byteMetaFileName[512];
            char dfgMetaFileName[512];
            char byteDataFileName[512];
            char dfgDataFileName[512];
            char stringFileName[512];
        };

        class AOTMetaInfo {
        public:
            AOTMetaInfo()
                : dataOffset(0)
                , dataSize(0)
                , aotType(AOT_NONE)
            {
            }

            AOTMetaInfo(int dataOffset, int dataSize, AOTType aotType)
                : dataOffset(dataOffset)
                , dataSize(dataSize)
                , aotType(aotType)
            {
            }

            int dataOffset;
            int dataSize;
            AOTType aotType;
        };  

    public:
        AOT(VM* vm);
        ~AOT();

        static Ref<AOT> create(VM* vm);

        inline bool isDFGAOTCed(AOTKey key) { return m_dfgMetaInfos.find(key) != m_dfgMetaInfos.end(); } 
        inline bool isByteAOTCed(AOTKey key) { return m_byteMetaInfos.find(key) != m_byteMetaInfos.end(); } 

        void storeFunctionByte(AOTKey key, UnlinkedFunctionExecutable* executable, UnlinkedFunctionCodeBlock* codeBlock);
        void storeProgramByte(AOTKey key, ProgramExecutable* executable, UnlinkedProgramCodeBlock* codeBlock);
        void storeDFG(AOTKey key, DFG::JITCompiler& jit, bool isGlobalCode = false);

        UnlinkedFunctionCodeBlock* loadFunctionByte(AOTKey key, UnlinkedFunctionExecutable* executable);
        UnlinkedProgramCodeBlock* loadProgramByte(AOTKey key, ProgramExecutable* executable);
        void loadDFG(AOTKey key, CodeBlock* codeBlock, bool);

        void loadMetaInfos(SourceProvider* provider);

        void storeStringTableMap();
        void loadStringTable(SourceProvider*);

        unsigned getFunctionIndex(void* func);
        void* getFunctionPtr(unsigned index);

        void printAOTCount();

    private:
        void writeToFile(AOTKey&, AOTMetaInfo&, bool);
        void loadFromFile(AOTKey&, AOTMetaInfo&, bool);

        void removeFiles(SourceProvider*);

        void registerFileNames(SourceProvider*);

        AOTStringTable* addStringTable(SourceProvider*);

        HashMap<AOTKey, AOTMetaInfo> m_dfgMetaInfos;
        HashMap<AOTKey, AOTMetaInfo> m_byteMetaInfos;

        HashMap<AOTKey, AOTMetaInfo> m_cachedByteMetaInfos;
        HashMap<AOTKey, AOTMetaInfo> m_cachedDFGMetaInfos;
        HashMap<RefPtr<SourceProvider>, int> m_offsetInByteDataFiles;
        HashMap<RefPtr<SourceProvider>, int> m_offsetInDFGDataFiles;
        HashMap<RefPtr<SourceProvider>, AOTFileName*> m_fileNames;

        typedef HashMap<RefPtr<SourceProvider>, RefPtr<AOTStringTable>> StringTableMap;
        StringTableMap m_stringTableMap;

        Vector<void*> m_functionList;

        VM* m_vm;

        AOTWriter* m_byteWriter;
        AOTWriter* m_dfgWriter;
        AOTLoader* m_byteLoader;
        AOTLoader* m_dfgLoader;

        bool m_storedOnce;

        unsigned m_storeByteAOTCount;
        unsigned m_storeDFGAOTCount;
        unsigned m_failedByteAOTCount;
        unsigned m_failedDFGAOTCount;
    };

    AOT* ensureGlobalAOT(VM* vm);
    AOT* existingGlobalAOT();

    bool recycleMode();
    void recycleModeSet(bool);
}
#endif

#endif
