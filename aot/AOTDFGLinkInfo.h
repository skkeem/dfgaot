#ifndef AOTDFGLinkInfo_h
#define AOTDFGLinkInfo_h

#if ENABLE(DFG_AOT)

#include <wtf/Vector.h>
#include "DFGNode.h"
#include "MacroAssembler.h"

namespace JSC {
struct AOTRelocTask;

struct AOTSwitchCase {

    AOTSwitchCase()
    {
    }
    
    AOTSwitchCase(unsigned lookup, unsigned target)
        : switchLookupValue(lookup)
        , targetIndex(target)
    {
    }

    unsigned switchLookupValue;
    unsigned targetIndex;
};

struct AOTSwitchData {
    AOTSwitchData()
        : kind(static_cast<DFG::SwitchKind>(-1))
        , switchTableIndex(UINT_MAX)
        , didUseJumpTable(false)
    {
    }

    AOTSwitchData(DFG::SwitchData& data)
        : fallThroughIndex(data.fallThrough.block->index)
        , kind(data.kind)
        , switchTableIndex(data.switchTableIndex)
        , didUseJumpTable(data.didUseJumpTable)
    {
    }
    
    unsigned fallThroughIndex;
    DFG::SwitchKind kind;
    unsigned switchTableIndex;
    bool didUseJumpTable;
};


struct AOTInRecord {
    AOTInRecord() {}

    AOTInRecord(
        MacroAssembler::PatchableJump jump, MacroAssembler::Label done,
        MacroAssembler::Call call, MacroAssembler::Label label, uint8_t stubIndex)
        : m_jump(jump)
        , m_done(done)
        , m_slowPathCall(call)
        , m_slowPathLabel(label)
        , m_stubInfoIndex(stubIndex)
    {
    }
    
    MacroAssembler::PatchableJump m_jump;
    MacroAssembler::Label m_done;

    MacroAssembler::Call m_slowPathCall;
    MacroAssembler::Label m_slowPathLabel;

    uint8_t m_stubInfoIndex;
};

struct AOTJSCallRecord {
    AOTJSCallRecord() {}

    AOTJSCallRecord(
        MacroAssembler::Call fastCall,
        MacroAssembler::Call slowCall,
        MacroAssembler::DataLabelPtr targetToCheck,
        uint8_t callLinkInfoIndex)
        : m_fastCall(fastCall)
        , m_slowCall(slowCall)
        , m_targetToCheck(targetToCheck)
        , m_callLinkInfoIndex(callLinkInfoIndex)
    {
    }
        
    MacroAssembler::Call m_fastCall;
    MacroAssembler::Call m_slowCall;
    MacroAssembler::DataLabelPtr m_targetToCheck;
    uint8_t m_callLinkInfoIndex;
};

struct AOTByIdRecord {
    AOTByIdRecord() {}

    AOTByIdRecord(
        MacroAssembler::DataLabel32 structureImm, 
        MacroAssembler::PatchableJump structureCheck,
        AssemblerLabel loadOrStore,
        AssemblerLabel tagLoadOrStore,
        MacroAssembler::Label done,
        MacroAssembler::Label slowPathBegin,
        MacroAssembler::Call call,
        uint8_t stubInfoIndex)
        : m_structureImm(structureImm)
        , m_structureCheck(structureCheck)
        , m_loadOrStore(loadOrStore)
        , m_tagLoadOrStore(tagLoadOrStore)
        , m_done(done)
        , m_slowPathBegin(slowPathBegin)
        , m_call(call)
        , m_stubInfoIndex(stubInfoIndex)
    {
    }

    MacroAssembler::DataLabel32 m_structureImm;
    MacroAssembler::PatchableJump m_structureCheck;
    AssemblerLabel m_loadOrStore;
#if USE(JSVALUE32_64)
    AssemblerLabel m_tagLoadOrStore;
#endif
    MacroAssembler::Label m_done;
    MacroAssembler::Label m_slowPathBegin;
    MacroAssembler::Call m_call;

    uint8_t m_stubInfoIndex;
};

struct AOTOSRExitCompilationInfo {
    AOTOSRExitCompilationInfo() 
    {
    }

    AOTOSRExitCompilationInfo(DFG::OSRExitCompilationInfo& info)
        : m_replacementSource(info.m_replacementSource)
        , m_replacementDestination(info.m_replacementDestination)
    {
    }

    MacroAssembler::Label m_replacementSource;
    MacroAssembler::Label m_replacementDestination;
};

struct AOTMinifiedNodeInfo {
    AOTMinifiedNodeInfo() 
    {
    }

    AOTMinifiedNodeInfo(unsigned index, AOTRelocTask task)
        : m_index(index)
        , m_task(task)
    {
    }

    unsigned m_index;
    AOTRelocTask m_task;
};

} // namespace JSC

#endif // ENABLE(DFG_AOT)

#endif // AOTDFGLinkInfo_h
