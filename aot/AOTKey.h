/*
 * Copyright (C) 2013, 2014 Apple Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#ifndef AOTKey_h
#define AOTKey_h

#if ENABLE(DFG_AOT)

#include <wtf/HashMap.h>

namespace JSC {

class SourceProvider;

enum AOTType {
    AOT_NONE,
    AOT_BYTE,
    AOT_DFG
};

class AOTKey {
public:
    AOTKey()
        : m_provider(0)
        , m_startOffset(0)
        , m_isCodeForConstruct(false)
    {
    }
    
    AOTKey(WTF::HashTableDeletedValueType)
        : m_provider(0)
        , m_startOffset(0)
        , m_isCodeForConstruct(false)
    {
    }
    
    AOTKey(SourceProvider* provider, int startOffset, bool isCodeForConstruct)
        : m_provider(provider)
        , m_startOffset(startOffset)
        , m_isCodeForConstruct(isCodeForConstruct)
    {
    }
    
    bool operator!() const
    {
        return !m_provider && !m_startOffset && !m_isCodeForConstruct;
    }
    
    bool isHashTableDeletedValue() const
    {
        return !m_provider && m_startOffset;
    }
    
    SourceProvider* provider() const { return m_provider; }
    int startOffset() const { return m_startOffset; }
    bool isCodeForConstruct() const { return m_isCodeForConstruct; }
    
    bool operator==(const AOTKey& other) const
    {
        return m_provider == other.m_provider
            && m_startOffset == other.m_startOffset
            && m_isCodeForConstruct == other.m_isCodeForConstruct;
    }
    
    unsigned hash() const
    {
        return WTF::pairIntHash(WTF::PtrHash<SourceProvider*>::hash(m_provider), m_startOffset);
    }
    

private:
    SourceProvider* m_provider;
    int m_startOffset;
    bool m_isCodeForConstruct;
};

struct AOTKeyHash {
    static unsigned hash(const AOTKey& key) { return key.hash(); }
    static bool equal(const AOTKey& a, const AOTKey& b) { return a == b; }
    static const bool safeToCompareToEmptyOrDeleted = true;
};

}  // namespace JSC

namespace WTF {

template<typename T> struct DefaultHash;
template<> struct DefaultHash<JSC::AOTKey> {
    typedef JSC::AOTKeyHash Hash;
};

template<typename T> struct HashTraits;
template<> struct HashTraits<JSC::AOTKey> : SimpleClassHashTraits<JSC::AOTKey> { };

} // namespace WTF

#endif // ENABLE(DFG_AOT)

#endif // AOTKey_h

