#ifndef AOTLoaderWriterInlines_h
#define AOTLoaderWriterInlines_h

#if ENABLE(BYTE_AOT) 

#include "AOTLoaderWriter.h"
#include "JSCJSValueInlines.h"
#include "UnlinkedCodeBlock.h"
#include "UnlinkedInstructionStream.h"
#include "SymbolTable.h"
#include "CodeType.h"
#include "AOTStringTable.h" 
#include "Strong.h"
#include "StrongInlines.h"
#include "IdentifierInlines.h"
#include "BuiltinNames.h"

namespace JSC {

bool AOTWriter::writeJSValue(JSValue value)
{
    EncodedJSValue encode = JSValue::encode(value);
    m_buffer.ensureSpace(sizeof(EncodedJSValue));
    m_buffer.put(encode);

    if (value.isCell()) {
        m_buffer.ensureSpace(sizeof(bool));
        if (value.isString()) {
            m_buffer.put(true);
            m_buffer.ensureSpace(sizeof(unsigned));
            if (value.getString(NULL).length()) {
                if (!m_stringTable->validate(value.getString(NULL).impl()))
                    return false;
                m_buffer.put(m_stringTable->add(value.getString(NULL).impl()));
            } else {
                m_buffer.put(UINT_MAX);
            }
        }
        else if (reinterpret_cast<SymbolTable*>(value.asCell())->info() == SymbolTable::info()) {
            m_buffer.put(false);
            if (!writeSymbolTable(value))
                return false;
        }
        else {
            puts("AOTStoreBuffer::putJSValue write JSValue that is not a String nor SymbolTable");
            RELEASE_ASSERT_NOT_REACHED();
        }
    }

    return true;
}

bool AOTWriter::writeUnlinkedFunctionExecutable(UnlinkedFunctionExecutable* func)
{
    //TODO
    m_buffer.ensureSpace(2*sizeof(bool) + 2*sizeof(unsigned));
    if (func->m_name.length()) {
        m_buffer.put(true);
        if (!m_stringTable->validate(func->m_name.impl()))
            return false;
        m_buffer.put(m_stringTable->add(func->m_name.impl()));
    } else {
        m_buffer.put(false);
    }
    if (func->m_inferredName.length()) {
        m_buffer.put(true);
        if (!m_stringTable->validate(func->m_inferredName.impl()))
            return false;
        m_buffer.put(m_stringTable->add(func->m_inferredName.impl()));
    } else {
        m_buffer.put(false);
    }

    m_buffer.ensureSpace(18 * sizeof(unsigned));
    m_buffer.put(func->m_firstLineOffset);
    m_buffer.put(func->m_lineCount);
    m_buffer.put(func->m_unlinkedFunctionNameStart);
    m_buffer.put(func->m_unlinkedBodyStartColumn);
    m_buffer.put(func->m_unlinkedBodyEndColumn);
    m_buffer.put(func->m_startOffset);
    m_buffer.put(func->m_sourceLength);
    m_buffer.put(func->m_parametersStartOffset);
    m_buffer.put(func->m_typeProfilingStartOffset);
    m_buffer.put(func->m_typeProfilingEndOffset);
    m_buffer.put(func->m_parameterCount);
    //m_buffer.put(func->m_features);
    m_buffer.put(func->m_isInStrictContext);
    //m_buffer.put(func->m_hasCapturedVariables);
    //m_buffer.put(func->m_isBuiltinFunction);
    m_buffer.put(func->m_constructAbility);
    m_buffer.put(func->m_constructorKind);
    m_buffer.put(func->m_functionMode);
    m_buffer.put(func->m_superBinding);
    m_buffer.put(func->m_derivedContextType);
    m_buffer.put(func->m_sourceParseMode);

    ASSERT(!((unsigned)func->m_features));
    ASSERT(!func->m_hasCapturedVariables);
    ASSERT(!func->m_isBuiltinFunction);
    ASSERT(!func->m_sourceOverride);    
    
    // m_nameValue is automatically created by finishCreation()

    if (!writeVariableEnvironment(func->parentScopeTDZVariables()))
        return false;

    return true;
}

bool AOTWriter::writeSymbolTable(JSValue value)
{
    typedef HashMap<RefPtr<UniquedStringImpl>, SymbolTableEntry, IdentifierRepHash, HashTraits<RefPtr<UniquedStringImpl>>, SymbolTableIndexHashTraits> Map;

    SymbolTable* symbolTable = reinterpret_cast<SymbolTable*>(value.asCell());

    unsigned size = symbolTable->size();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(size);
    for (Map::iterator iter = symbolTable->m_map.begin(), end = symbolTable->m_map.end(); iter != end; ++iter) {
        //TODO
        UniquedStringImpl* impl = iter->key.get();

        //TODO SymbolTableEntry
        m_buffer.ensureSpace(5*sizeof(unsigned));
        m_buffer.put((unsigned)iter->value.varOffset().m_kind);
        m_buffer.put(iter->value.varOffset().m_offset);
        m_buffer.put(iter->value.getAttributes());

        if (LIKELY(impl->isAtomic())) {
            if (!m_stringTable->validate(impl))
                return false;
            m_buffer.put(m_stringTable->add(impl));
        } else {
            ASSERT(impl->isSymbol());
            ASSERT(impl->is8Bit());
            ASSERT(impl->length());
            m_buffer.put((unsigned) UINT_MAX);
            m_buffer.put(m_vm->propertyNames->builtinNames().findSymbolIndex(impl));
        }

    }

    //TODO ignore the m_typeProfiliingRareData; it is handled in CodeBlock constructor

    m_buffer.ensureSpace(2*sizeof(bool) + 2*sizeof(unsigned));
    m_buffer.put(symbolTable->m_usesNonStrictEval);
    m_buffer.put(symbolTable->m_nestedLexicalScope);
    m_buffer.put((unsigned) symbolTable->m_scopeType);
    m_buffer.put(symbolTable->m_maxScopeOffset.offsetUnchecked());

    unsigned argumentsLength = symbolTable->argumentsLength();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(argumentsLength);
    m_buffer.ensureSpace(argumentsLength * sizeof(unsigned));
    for (unsigned i=0; i<argumentsLength; ++i) {
        m_buffer.put(symbolTable->argumentOffset(i).offsetUnchecked());
    }
    //TODO ignore the m_singletonScope;
    //TODO ignore the m_localToEntry;

    return true;
}

bool AOTWriter::writeVariableEnvironment(const VariableEnvironment* environment)
{
    typedef HashMap<RefPtr<UniquedStringImpl>, VariableEnvironmentEntry, IdentifierRepHash, HashTraits<RefPtr<UniquedStringImpl>>, VariableEnvironmentEntryHashTraits> Map;
    unsigned size = environment->size();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(size);

    for (Map::const_iterator iter = environment->begin(), end = environment->end(); iter != end; ++iter) {
        //TODO
        UniquedStringImpl* impl = iter->key.get();
        if (!m_stringTable->validate(impl))
            return false;

        m_buffer.ensureSpace(2*sizeof(unsigned));
        m_buffer.put(m_stringTable->add(impl));
        m_buffer.put((unsigned)iter->value.m_bits);
    }

    m_buffer.ensureSpace(sizeof(bool));
    m_buffer.put(environment->m_isEverythingCaptured);

    return true;
}

bool AOTWriter::writeUnlinkedCodeBlock(UnlinkedCodeBlock* codeBlock)
{
    const UnlinkedInstructionStream& instructions = codeBlock->instructions();
    const RefCountedArray<unsigned char>& instruction_data = instructions.data();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(instructions.count());
    m_buffer.put(instruction_data.data(), instruction_data.size());

    m_buffer.ensureSpace(7*sizeof(int));
    m_buffer.ensureSpace(16*sizeof(unsigned));

    m_buffer.put(codeBlock->m_numVars); //INT
    m_buffer.put(codeBlock->m_numCapturedVars); //INT
    m_buffer.put(codeBlock->m_numCalleeLocals); //INT

    m_buffer.put(codeBlock->m_numParameters); //INT

    m_buffer.put(codeBlock->thisRegister().offset()); //INT
    m_buffer.put(codeBlock->scopeRegister().offset()); //INT
    m_buffer.put(codeBlock->globalObjectRegister().offset()); //INT

    m_buffer.put(codeBlock->m_usesEval);
    m_buffer.put(codeBlock->m_isStrictMode);
    m_buffer.put(codeBlock->m_isConstructor);
    m_buffer.put(codeBlock->m_hasCapturedVariables);
    m_buffer.put(codeBlock->m_isBuiltinFunction);
    m_buffer.put(codeBlock->m_constructorKind);
    m_buffer.put(codeBlock->m_superBinding);
    m_buffer.put(codeBlock->m_derivedContextType);
    m_buffer.put(codeBlock->m_isArrowFunctionContext);
    m_buffer.put(codeBlock->m_isClassContext);
    m_buffer.put(codeBlock->m_firstLine);
    m_buffer.put(codeBlock->m_lineCount);
    m_buffer.put(codeBlock->m_endColumn);

    m_buffer.put((unsigned)codeBlock->m_parseMode);
    m_buffer.put((unsigned)codeBlock->m_features);
    m_buffer.put((unsigned)codeBlock->m_codeType);

    m_buffer.putVector(codeBlock->m_jumpTargets);

    m_buffer.putVector(codeBlock->m_propertyAccessInstructions);

    //TODO check putIdentifier
    Vector<Identifier>& identifiers = codeBlock->m_identifiers;
    Vector<unsigned> identifierIndexVector;
    Vector<unsigned> identifierSymbolIndexVector;
    for (unsigned i=0; i<identifiers.size(); i++) {
        StringImpl* impl = identifiers[i].impl();
        if (LIKELY(impl->isAtomic())) {
            if (!m_stringTable->validate(identifiers[i].impl()))
                return false;
            identifierIndexVector.append(m_stringTable->add(identifiers[i].impl()));
        } else {
            ASSERT(impl->isSymbol());
            ASSERT(impl->is8Bit());
            ASSERT(impl->length());
            identifierIndexVector.append(UINT_MAX);
            identifierSymbolIndexVector.append(m_vm->propertyNames->builtinNames().findSymbolIndex(impl));
        }
    }
    m_buffer.putVector(identifierIndexVector);
    m_buffer.putVector(identifierSymbolIndexVector);

    const Vector<WriteBarrier<Unknown>>& constantRegisters = codeBlock->constantRegisters();
    unsigned numberOfConstantRegisters = constantRegisters.size();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfConstantRegisters);
    for (unsigned i=0; i<numberOfConstantRegisters; i++) {
        if (!writeJSValue(constantRegisters[i].get()))
           return false; 
    }

    m_buffer.putVector(codeBlock->m_constantsSourceCodeRepresentation);

    unsigned numberOfFunctionDecls = codeBlock->numberOfFunctionDecls();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfFunctionDecls);
    for (unsigned i=0; i<numberOfFunctionDecls; i++) {
        UnlinkedFunctionExecutable* func = codeBlock->functionDecl(i);
        if (!writeUnlinkedFunctionExecutable(func))
            return false;
    }

    unsigned numberOfFunctionExprs = codeBlock->numberOfFunctionExprs();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfFunctionExprs);
    for (unsigned i=0; i<numberOfFunctionExprs; i++) {
        UnlinkedFunctionExecutable* func = codeBlock->functionExpr(i);
        if (!writeUnlinkedFunctionExecutable(func))
            return false;
    }

    unsigned numberOfLinkTimeConstants = codeBlock->m_linkTimeConstants.size();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfLinkTimeConstants);
    m_buffer.ensureSpace(numberOfLinkTimeConstants * sizeof(unsigned));
    for (unsigned i=0; i<numberOfLinkTimeConstants; i++) {
        m_buffer.put(codeBlock->m_linkTimeConstants[i]);
    }

    m_buffer.ensureSpace(5 * sizeof(unsigned));
    m_buffer.put(codeBlock->m_arrayProfileCount);
    m_buffer.put(codeBlock->m_arrayAllocationProfileCount);
    m_buffer.put(codeBlock->m_objectAllocationProfileCount);
    m_buffer.put(codeBlock->m_valueProfileCount);
    m_buffer.put(codeBlock->m_llintCallLinkInfoCount);

    m_buffer.putVector(codeBlock->m_expressionInfo);

    m_buffer.ensureSpace(sizeof(bool));
    m_buffer.put(codeBlock->hasRareData());
    if (!codeBlock->hasRareData())
        return true;

    // RareData
    m_buffer.putVector(codeBlock->m_rareData->m_exceptionHandlers);
    
    unsigned numberOfRegExps = codeBlock->numberOfRegExps();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfRegExps);
    for (unsigned i=0; i<numberOfRegExps; i++) {
        RegExpKey regExpKey = codeBlock->regexp(i)->key();
        m_buffer.ensureSpace(sizeof(int) + sizeof(unsigned));
        m_buffer.put(static_cast<int>(regExpKey.flagsValue));
        //TODO
        if (!m_stringTable->validate(regExpKey.pattern.get()))
            return false;
        m_buffer.put(m_stringTable->add(regExpKey.pattern.get()));
    }
    
    unsigned numberOfConstantBuffers = codeBlock->constantBufferCount();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfConstantBuffers);
    for (unsigned i=0; i<numberOfConstantBuffers; i++) {
        const Vector<JSValue>& constantBuffer = codeBlock->constantBuffer(i);
        unsigned constantBufferSize = constantBuffer.size();
        m_buffer.ensureSpace(sizeof(unsigned));
        m_buffer.put(constantBufferSize);
        for (unsigned j=0; j<constantBufferSize; j++) {
            if (!writeJSValue(constantBuffer[j]))
                return false;
        }
    }
    
    unsigned numberOfSwitchJumpTables = codeBlock->numberOfSwitchJumpTables();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfSwitchJumpTables);
    for (unsigned i=0; i<numberOfSwitchJumpTables; i++) {
        const UnlinkedSimpleJumpTable& jumpTable = codeBlock->switchJumpTable(i);
        m_buffer.ensureSpace(sizeof(int));
        m_buffer.put((int)jumpTable.min);
        m_buffer.putVector(jumpTable.branchOffsets);
    }

    unsigned numberOfStringSwitchJumpTables = codeBlock->numberOfStringSwitchJumpTables();
    m_buffer.ensureSpace(sizeof(unsigned));
    m_buffer.put(numberOfStringSwitchJumpTables);
    for (unsigned i=0; i<numberOfStringSwitchJumpTables; i++) {
        UnlinkedStringJumpTable::StringOffsetTable& offsetTable = codeBlock->stringSwitchJumpTable(i).offsetTable;
        unsigned offsetTableSize = offsetTable.size();
        m_buffer.ensureSpace(sizeof(unsigned));
        m_buffer.put(offsetTableSize);
        if (offsetTableSize) {
            UnlinkedStringJumpTable::StringOffsetTable::const_iterator iter = offsetTable.begin();
            UnlinkedStringJumpTable::StringOffsetTable::const_iterator end = offsetTable.end();
            for (; iter != end; ++iter) {
                //TODO
                if (!m_stringTable->validate(iter->key.get()))
                    return false;
                m_buffer.ensureSpace(sizeof(unsigned) + sizeof(int));
                m_buffer.put(m_stringTable->add(iter->key.get()));
                m_buffer.put((int)iter->value);
            }
        }
    }

    m_buffer.putVector(codeBlock->expressionInfoFatPositions());

    // TODO check m_rareData->m_typeProfilerInfoMap 
    // TODO check m_rareData->m_opProfileControlFlowBytecodeOffsets

    return true;
}

bool AOTWriter::writeFunctionCodeBlock(UnlinkedFunctionExecutable* executable, UnlinkedFunctionCodeBlock* codeBlock)
{
    if (!writeUnlinkedCodeBlock(codeBlock))
        return false;

    m_buffer.ensureSpace(sizeof(unsigned) + sizeof(bool));
    m_buffer.put((unsigned) executable->features());
    m_buffer.put(executable->hasCapturedVariables());

    return true;
}

bool AOTWriter::writeProgramCodeBlock(ProgramExecutable* executable, UnlinkedProgramCodeBlock* codeBlock)
{
    if (!writeUnlinkedCodeBlock(codeBlock))
        return false;

    if (!writeVariableEnvironment(&codeBlock->m_varDeclarations))
        return false;
    if (!writeVariableEnvironment(&codeBlock->m_lexicalDeclarations))
        return false;

    m_buffer.ensureSpace(sizeof(bool) + 2*sizeof(int) + 3*sizeof(unsigned));
    m_buffer.put((unsigned) executable->features());
    m_buffer.put(executable->hasCapturedVariables());
    m_buffer.put(executable->firstLine());
    m_buffer.put(executable->lastLine());
    m_buffer.put(executable->startColumn());
    m_buffer.put(executable->endColumn());

    return true;
}

JSValue AOTLoader::loadJSValue()
{
    JSValue value = m_buffer.getJSValue();
    if (value.isCell()) {
        // is String?
        if (m_buffer.getBool()) {
            unsigned stringIndex = m_buffer.getUnsigned();
            return JSValue(jsString(m_vm, String(m_stringTable->get(stringIndex))));
        } else {
            return loadSymbolTable();
        }
    } 

    return value;
}

JSValue AOTLoader::loadSymbolTable()
{
    SymbolTable* symbolTable = SymbolTable::create(*m_vm);

    unsigned size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        VarKind kind = (VarKind) m_buffer.getUnsigned();
        unsigned offset = m_buffer.getUnsigned();
        unsigned attribute = m_buffer.getUnsigned();

        unsigned stringIndex = m_buffer.getUnsigned();
        StringImpl* impl;
        if (LIKELY(stringIndex < UINT_MAX)) 
            impl = m_stringTable->get(stringIndex);
        else 
            impl = m_vm->propertyNames->builtinNames().getSymbol(m_buffer.getUnsigned());
        
        SymbolTableEntry entry(VarOffset::assemble(kind, offset), attribute); 
        symbolTable->add(reinterpret_cast<UniquedStringImpl*>(impl), entry);
    }

    symbolTable->m_usesNonStrictEval = m_buffer.getBool();
    symbolTable->m_nestedLexicalScope = m_buffer.getBool();
    symbolTable->m_scopeType = (SymbolTable::ScopeType) m_buffer.getUnsigned();
    symbolTable->m_maxScopeOffset = ScopeOffset(m_buffer.getUnsigned());

    size = m_buffer.getUnsigned();
    symbolTable->setArgumentsLength(*m_vm, size);
    for (unsigned i=0; i<size; ++i) {
        ScopeOffset offset(m_buffer.getUnsigned());
        symbolTable->setArgumentOffset(*m_vm, i, offset);
    }

    return JSValue(symbolTable);
}

void AOTLoader::loadVariableEnvironment(VariableEnvironment& environment)
{
    unsigned size = m_buffer.getUnsigned();
    unsigned stringIndex;
    for (unsigned i=0; i<size; i++) {
        stringIndex = m_buffer.getUnsigned();
        environment.add(reinterpret_cast<UniquedStringImpl*>(m_stringTable->get(stringIndex)), (uint8_t)m_buffer.getUnsigned());
    }

    environment.m_isEverythingCaptured = m_buffer.getBool();
}

UnlinkedFunctionExecutable* AOTLoader::loadUnlinkedFunctionExecutable()
{
    Identifier name = (m_buffer.getBool())? Identifier(m_vm, m_stringTable->get(m_buffer.getUnsigned())) : m_vm->propertyNames->nullIdentifier;
    Identifier inferredName = (m_buffer.getBool())? Identifier(m_vm, m_stringTable->get(m_buffer.getUnsigned())) : m_vm->propertyNames->nullIdentifier;
    
    UnlinkedFunctionExecutable* func = UnlinkedFunctionExecutable::create(m_vm, name, inferredName);

    func->m_firstLineOffset = m_buffer.getUnsigned();
    func->m_lineCount = m_buffer.getUnsigned();
    func->m_unlinkedFunctionNameStart = m_buffer.getUnsigned();
    func->m_unlinkedBodyStartColumn = m_buffer.getUnsigned();
    func->m_unlinkedBodyEndColumn = m_buffer.getUnsigned();
    func->m_startOffset = m_buffer.getUnsigned();
    func->m_sourceLength = m_buffer.getUnsigned();
    func->m_parametersStartOffset = m_buffer.getUnsigned();
    func->m_typeProfilingStartOffset = m_buffer.getUnsigned();
    func->m_typeProfilingEndOffset = m_buffer.getUnsigned();
    func->m_parameterCount = m_buffer.getUnsigned();
    //put(func->m_features);
    func->m_isInStrictContext = m_buffer.getUnsigned();
    //put(func->m_hasCapturedVariables);
    //put(func->m_isBuiltinFunction);
    func->m_constructAbility = m_buffer.getUnsigned();
    func->m_constructorKind = m_buffer.getUnsigned();
    func->m_functionMode = m_buffer.getUnsigned();
    func->m_superBinding = m_buffer.getUnsigned();
    func->m_derivedContextType = m_buffer.getUnsigned();
    func->m_sourceParseMode = m_buffer.getUnsigned();

    //m_sourceOverride is NULL in default;
    
    VariableEnvironment environment;
    loadVariableEnvironment(environment);
    func->m_parentScopeTDZVariables.swap(environment);

    return func;
}

UnlinkedFunctionCodeBlock* AOTLoader::loadFunctionCodeBlock(UnlinkedFunctionExecutable* executable)
{
    UnlinkedFunctionCodeBlock* codeBlock = UnlinkedFunctionCodeBlock::create(m_vm);
    Strong<UnlinkedFunctionCodeBlock> strong(*m_vm, codeBlock);
    
    loadUnlinkedCodeBlock(codeBlock);

    executable->recordParse((uint16_t)m_buffer.getUnsigned(), m_buffer.getBool());

    return codeBlock;
}

UnlinkedProgramCodeBlock* AOTLoader::loadProgramCodeBlock(ProgramExecutable* executable)
{
    UnlinkedProgramCodeBlock* codeBlock = UnlinkedProgramCodeBlock::create(m_vm);
    Strong<UnlinkedProgramCodeBlock> strong(*m_vm, codeBlock);

    loadUnlinkedCodeBlock(codeBlock);

    VariableEnvironment varDeclarations;
    VariableEnvironment lexicalDeclarations;
    loadVariableEnvironment(varDeclarations);
    loadVariableEnvironment(lexicalDeclarations);
    codeBlock->m_varDeclarations.swap(varDeclarations);
    codeBlock->m_lexicalDeclarations.swap(lexicalDeclarations);

    executable->recordParse((uint16_t)m_buffer.getUnsigned(), m_buffer.getBool(), m_buffer.getInt(), m_buffer.getInt(), m_buffer.getUnsigned(), m_buffer.getUnsigned());

    return codeBlock;
}

void AOTLoader::loadUnlinkedCodeBlock(UnlinkedCodeBlock* codeBlock)
{
    unsigned size;

    unsigned instructionCount =  m_buffer.getUnsigned();
    Vector<unsigned char> instruction_vector;
    m_buffer.getVector(instruction_vector);
    codeBlock->setInstructions(std::make_unique<UnlinkedInstructionStream>(instruction_vector, instructionCount));

    codeBlock->m_numVars = m_buffer.getInt();
    codeBlock->m_numCapturedVars = m_buffer.getInt();
    codeBlock->m_numCalleeLocals = m_buffer.getInt();

    codeBlock->m_numParameters= m_buffer.getInt();

    codeBlock->m_thisRegister.m_virtualRegister = m_buffer.getInt();
    codeBlock->m_scopeRegister.m_virtualRegister = m_buffer.getInt();
    codeBlock->m_globalObjectRegister.m_virtualRegister = m_buffer.getInt();

    codeBlock->m_usesEval = m_buffer.getUnsigned();
    codeBlock->m_isStrictMode = m_buffer.getUnsigned();
    codeBlock->m_isConstructor = m_buffer.getUnsigned();
    codeBlock->m_hasCapturedVariables = m_buffer.getUnsigned();
    codeBlock->m_isBuiltinFunction = m_buffer.getUnsigned();
    codeBlock->m_constructorKind = m_buffer.getUnsigned();
    codeBlock->m_superBinding = m_buffer.getUnsigned();
    codeBlock->m_derivedContextType = m_buffer.getUnsigned();
    codeBlock->m_isArrowFunctionContext = m_buffer.getUnsigned();
    codeBlock->m_isClassContext = m_buffer.getUnsigned();
    codeBlock->m_firstLine = m_buffer.getUnsigned();
    codeBlock->m_lineCount = m_buffer.getUnsigned();
    codeBlock->m_endColumn = m_buffer.getUnsigned();

    codeBlock->m_parseMode = (SourceParseMode) m_buffer.getUnsigned();
    codeBlock->m_features = m_buffer.getUnsigned();
    codeBlock->m_codeType = (CodeType) m_buffer.getUnsigned();

    m_buffer.getVector(codeBlock->m_jumpTargets);

    m_buffer.getVector(codeBlock->m_propertyAccessInstructions);

    //TODO check putIdentifier
    Vector<unsigned> identifierIndexVector;
    Vector<unsigned> identifierSymbolIndexVector;
    m_buffer.getVector(identifierIndexVector);
    m_buffer.getVector(identifierSymbolIndexVector);
    unsigned symbolIndex = 0;
    for (unsigned i=0; i<identifierIndexVector.size(); i++) {
        unsigned index = identifierIndexVector[i];
        StringImpl* impl;
        if (LIKELY(index < UINT_MAX)) {
            impl = m_stringTable->get(index);
            codeBlock->addIdentifier(Identifier(m_vm, impl)); 
        } else {
            impl = m_vm->propertyNames->builtinNames().getSymbol(identifierSymbolIndexVector[symbolIndex++]);
            codeBlock->addIdentifier(Identifier(static_cast<SymbolImpl&>(*impl)));
        }
    }

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        codeBlock->addConstant(loadJSValue());
    }

    codeBlock->m_constantsSourceCodeRepresentation.clear();
    m_buffer.getVector(codeBlock->m_constantsSourceCodeRepresentation);

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        codeBlock->addFunctionDecl(loadUnlinkedFunctionExecutable());
    }

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        codeBlock->addFunctionExpr(loadUnlinkedFunctionExecutable());
    }


    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        codeBlock->m_linkTimeConstants[i] = m_buffer.getUnsigned();
    }

    codeBlock->m_arrayProfileCount = m_buffer.getUnsigned();
    codeBlock->m_arrayAllocationProfileCount = m_buffer.getUnsigned();
    codeBlock->m_objectAllocationProfileCount = m_buffer.getUnsigned();
    codeBlock->m_valueProfileCount = m_buffer.getUnsigned();
    codeBlock->m_llintCallLinkInfoCount = m_buffer.getUnsigned();

    m_buffer.getVector(codeBlock->m_expressionInfo);

    if (!m_buffer.getBool()) return;

    // RareData
    codeBlock->createRareDataIfNecessary();

    m_buffer.getVector(codeBlock->m_rareData->m_exceptionHandlers);
    
    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        RegExpFlags flags = (RegExpFlags) m_buffer.getInt();
        unsigned stringIndex = m_buffer.getUnsigned();
        codeBlock->addRegExp(RegExp::create(*m_vm, String(m_stringTable->get(stringIndex)), flags));
    }

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        unsigned bufferSize = m_buffer.getUnsigned();
        codeBlock->addConstantBuffer(bufferSize);
        Vector<JSValue>& buffer = codeBlock->constantBuffer(i);
        for (unsigned j=0; j<bufferSize; j++) {
            buffer[j] = loadJSValue();
        }
    }

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        UnlinkedSimpleJumpTable& table = codeBlock->addSwitchJumpTable();
        table.min = m_buffer.getInt();
        m_buffer.getVector(table.branchOffsets);
    }

    size = m_buffer.getUnsigned();
    for (unsigned i=0; i<size; i++) {
        UnlinkedStringJumpTable& table = codeBlock->addStringSwitchJumpTable();
        unsigned offsetTableSize = m_buffer.getUnsigned();
        for (unsigned j=0; j<offsetTableSize; j++) {
            unsigned stringIndex = m_buffer.getUnsigned();
            int32_t value = m_buffer.getInt();
            table.offsetTable.add(m_stringTable->get(stringIndex), value);
        }
    }
            
    m_buffer.getVector(codeBlock->expressionInfoFatPositions());

    // TODO check m_rareData->m_typeProfilerInfoMap / m_opProfileControlFlowBytecodeOffsets

}

}

#endif
/* END BYTE_AOT */
#endif
