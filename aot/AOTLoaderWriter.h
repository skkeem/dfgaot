#ifndef AOTLoaderWriter_h
#define AOTLoaderWriter_h

#if ENABLE(DFG_AOT) 

#include "AOTKey.h"
#include "MacroAssembler.h"
#include <wtf/Vector.h>
#include <wtf/text/StringCommon.h>

#if USE(JSVALUE32_64)
#include <wtf/Bag.h>
#include <unordered_map>
#endif

#define AOTBUFFERSIZE 1024*10

namespace JSC {
    class VM;
    class AOT;
    class JSValue;
    class Identifier;
    class LinkBuffer;
    class CodeBlock;
    class AOTStringTable;
    class VariableEnvironment;
    class UnlinkedCodeBlock;
    class UnlinkedFunctionCodeBlock;
    class UnlinkedProgramCodeBlock;
    class UnlinkedFunctionExecutable;
    class ProgramExecutable;

    namespace DFG {
        class JITCompiler;
        class JumpReplacement;
        class SpeculationRecovery;
        class VariableEvent;
        class MinifiedNode;
        class JITCode;
    }

    class AOTStoreBuffer {
    public:
        AOTStoreBuffer();

        char* data() const { return m_buffer; }
        int size() const { return m_index; }

        bool isAvailable(int space)
        {
            return m_index + space <= m_capacity;
        }

        void ensureSpace(int space)
        {
            while (!isAvailable(space)) grow();
        }

        void grow(int extraCapacity = 0);
        void reset();

        template<typename IntegralType>
        void put(IntegralType value);

        template<typename IntegralType>
        void put(IntegralType* data, int size);

        template<typename IntegralType>
        void putVector(const Vector<IntegralType>& vector);

    private:
        Vector<char, AOTBUFFERSIZE> m_storage;
        char* m_buffer;
        int m_capacity;
        int m_index;
    };

    class AOTLoadBuffer {
    public:
        AOTLoadBuffer();

        char* data() const { return m_buffer; }
        int index() const { return m_index; }

        void resize(int size) {
            m_storage.resize(std::max(size, AOTBUFFERSIZE)); 
            m_index = 0;
            m_buffer = m_storage.begin();
        }

        bool getBool();
        int getInt();
        unsigned getUnsigned();
        JSValue getJSValue();

        void getData(void* start, size_t size);
        char* getData(size_t size);

        template<typename IntegralType>
        void getVector(Vector<IntegralType>& vector);

    private:
        Vector<char, AOTBUFFERSIZE> m_storage;
        char* m_buffer;
        int m_index;
    };

    class AOTWriter {
        typedef MacroAssembler::Call Call;
        typedef MacroAssembler::Label Label;
    public:
        AOTWriter(VM* vm, AOT* aot);
        ~AOTWriter();

        char* bufferData() { return m_buffer.data(); }
        int bufferSize() { return m_buffer.size(); }

        void clear() { m_buffer.reset(); }

        bool writeFunctionCodeBlock(UnlinkedFunctionExecutable*, UnlinkedFunctionCodeBlock*);
        bool writeProgramCodeBlock(ProgramExecutable*, UnlinkedProgramCodeBlock*);

        void writeDFGJITCode(DFG::JITCompiler&);
        bool writeDFGJITInfo(DFG::JITCompiler&, bool);

        void setAOTStringTable(AOTStringTable* table) 
        {
            ASSERT(table);
            m_stringTable = table; 
        }

    private:
        VM* m_vm;
        AOT* m_aot;
        AOTStoreBuffer m_buffer;
        AOTStringTable* m_stringTable;
        //AOTType m_aotType;

        bool writeJSValue(JSValue);
        bool writeSymbolTable(JSValue);
        bool writeVariableEnvironment(const VariableEnvironment*);
        bool writeUnlinkedCodeBlock(UnlinkedCodeBlock*);
        bool writeUnlinkedFunctionExecutable(UnlinkedFunctionExecutable*);

        void writeDFGCodeBlockInfo(CodeBlock*);
        void writeDFGJITCodeInfo(DFG::JITCompiler&);
    };


    class AOTLoader {
        typedef MacroAssembler::Call Call;
        typedef MacroAssembler::Label Label;
    public:
        AOTLoader(VM* vm, AOT* aot);
        ~AOTLoader();

        int bufferIndex() { return m_buffer.index(); }

        void loadDataFile(FILE*, int);

        UnlinkedFunctionCodeBlock* loadFunctionCodeBlock(UnlinkedFunctionExecutable*);
        UnlinkedProgramCodeBlock* loadProgramCodeBlock(ProgramExecutable*);

        void loadDFGJITCode(LinkBuffer&, CodeBlock*);
        void loadDFGJITInfo(LinkBuffer&, CodeBlock*, bool);
        void finalize(LinkBuffer&, CodeBlock*);
        void finalizeFunction(LinkBuffer&, CodeBlock*);

        void setAOTStringTable(AOTStringTable* table) 
        {
            ASSERT(table);
            m_stringTable = table; 
        }

#if USE(JSVALUE32_64)
        void* addressOfDoubleConstant(double value)
        {
            int64_t valueBits = bitwise_cast<int64_t>(value);
            auto it = m_doubleConstantsMap.find(valueBits);
            if (it != m_doubleConstantsMap.end())
                return it->second;

            if (!m_doubleConstants)
                m_doubleConstants = std::make_unique<Bag<double>>();
        
            double* addressInConstantPool = m_doubleConstants->add();
            *addressInConstantPool = value;
            m_doubleConstantsMap[valueBits] = addressInConstantPool;
            return addressInConstantPool;
        }
#endif

    private:
        VM* m_vm;
        AOT* m_aot;
        AOTLoadBuffer m_buffer;
        AOTStringTable* m_stringTable;

        // m_jitCode data
        Vector<DFG::OSRExit> m_osrExit;
        Vector<DFG::SpeculationRecovery> m_speculationRecovery;
        Vector<DFG::VariableEvent> m_variableEvents;
        Vector<DFG::MinifiedNode> m_minifiedNodes;

        // m_jitCode->common data
        unsigned m_frameRegisterCount;
        unsigned m_requiredRegisterCountForExit;
        Vector<DFG::JumpReplacement> m_jumpReplacements;
#if USE(JSVALUE32_64)
        std::unique_ptr<Bag<double>> m_doubleConstants;
        std::unordered_map<int64_t, double*> m_doubleConstantsMap;
#endif

        MacroAssemblerCodePtr m_withArityCheck;

        JSValue loadJSValue();
        JSValue loadSymbolTable();
        void loadVariableEnvironment(VariableEnvironment&);
        UnlinkedFunctionExecutable* loadUnlinkedFunctionExecutable();
        void loadUnlinkedCodeBlock(UnlinkedCodeBlock*);

        void loadDFGCodeBlockInfo(CodeBlock*);
        void loadDFGJITCodeInfo(LinkBuffer&, CodeBlock*);
        void relocateAOTRelocTasks(LinkBuffer&, CodeBlock*);

        void setJITCodeInfo(DFG::JITCode*);
    };

}
#endif

#endif
