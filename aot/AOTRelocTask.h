#ifndef AOTRelocTask_h
#define AOTRelocTask_h

#if ENABLE(DFG_AOT)

//#define PUT(TYPE, ATTR)  putRelocTask(AOTRelocTask(m_currentIndex, AOTRelocTask::TYPE, (ATTR)))
#define PUT(TYPE, ATTR)  putRelocTask(TASK(m_currentIndex, TYPE, (ATTR)))
#define TASK(BC, TYPE, ATTR) (AOTRelocTask((BC), AOTRelocTask::TYPE, (ATTR)))
#define TASKD(BC, TYPE, ATTR, VALUE) (AOTRelocTask((BC), AOTRelocTask::TYPE, (ATTR), (VALUE)))
#define TASKP(TSK, MC) (AOTRelocTask((TSK), (MC)))
#define TASKC(TSK, TYPE) (AOTRelocTask((TSK), AOTRelocTask::TYPE))

namespace JSC {

struct AOTRelocTask {
    enum RelocType {
        INVALID,
        Null,
        VM,
        FuncDecl,
        FuncExpr,
        Local,
        CodeBlock,
        StackLimit,
        TopCallFrame,
        Exception,
        SymTable,
        Pointer,
        PointerTag,
        SpecialPointer,
        Regexp,
        CopiedAllocatorRemaining,
        CopiedAllocatorPayloadEnd,
        GenFuncAllocator,
        FuncAllocator,
        ObjAllocator,
        LEAllocator,
        JSRopeAllocator,
        DAAllocator,
        StringObjAllocator,
        JSArrayBVAllocator,
        JSArrayAllocator,
        DASpace,
        FuncStructure,
        GenFuncStructure,
        ObjAllocStructure,
        LEStructure,
        StrStructure,
        DAStructure,
        ProtoHasInstanceSymbolFunction,
        ConstantCell,
        ConstantScope,
        GlobalObject,
        GlobalLexicalEnvironment,
        ArrayStructure,
        StringObjectStructure,
        TypedArrayStructure,
        ScratchBuffer,
        CreateScratchBuffer,
        EndOfScratchBuffer,
        ScratchBufferIdx,
        ScratchBufferIdxTag,
        ScratchBufferIdxPayload,
        ScratchBufferALP,
        TypeProfilerLog,
        TypeProfilerLogLEP,
        WatchDogDFA,
        SingleCharStr,
        TwoToThe32,
        Num,
        WriteBarrierBuffer,
        OSRExitIndex,
        StringObjectInfo,
        SwitchJumpTable,
        CallLinkInfo,
        StubInfo,
        StringImpl,
        SymbolStringImpl,
        ToThis,
        DoubleConstant,
        PCForThrow
    };

    AOTRelocTask()
        : type(INVALID)
    {
    }

    AOTRelocTask(unsigned bc, RelocType type, int attr)
        : bc(bc)
        , type(type)
        , attr(attr)
        , mc(0)
    {
    }

    AOTRelocTask(unsigned bc, RelocType type, int attr, double value)
        : bc(bc)
        , type(type)
        , attr(attr)
        , doubleValue(value)
        , mc(0)
    {
    }

    AOTRelocTask(const AOTRelocTask& other)
        : bc(other.bc)
        , type(other.type)
        , attr(other.attr)
        , doubleValue(other.doubleValue)
        , mc(other.mc)
    {
    }

    AOTRelocTask(const AOTRelocTask& other, unsigned offset)
        : bc(other.bc)
        , type(other.type)
        , attr(other.attr)
        , doubleValue(other.doubleValue)
        , mc(offset)
    {
    }
    
    AOTRelocTask(const AOTRelocTask& other, RelocType typeChange)
        : bc(other.bc)
        , type(typeChange)
        , attr(other.attr)
        , doubleValue(other.doubleValue)
        , mc(other.mc)
    {
    }

    bool isValid() { return type != INVALID; }

    unsigned bc; // where this is in bytecode
    RelocType type; // What this is.
    int attr; // Specify what this is.
    double doubleValue; // for double constant value
    unsigned mc; // where this is in machine code.
};

} // namespace JSC

#endif // ENABLE(DFG_AOT)

#endif // AOTRelocTask_h
